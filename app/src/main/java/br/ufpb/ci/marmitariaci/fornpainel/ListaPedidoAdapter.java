package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;
import br.ufpb.ci.marmitariaci.data.Pedido;

public class ListaPedidoAdapter extends RecyclerView.Adapter<ListaPedidoAdapter.PedidoViewHolder>{

    private List<Pedido> mListaPedido;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public ListaPedidoAdapter(Context context, List<Pedido> listaPedido){
        this.mListaPedido = listaPedido;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }


    @NonNull
    @Override
    public PedidoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_pedido, parent, false);
        PedidoViewHolder pedidoViewHolder = new PedidoViewHolder(view);
        return pedidoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PedidoViewHolder holder, int position) {
        holder.mId.setText("Pedido: " + String.valueOf(mListaPedido.get(position).getId()));
    }

    public void setmRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        this.mRecyclerViewOnClickListenerHack = r;
    }

    public Pedido getItem(int position){
        return mListaPedido.get(position);
    }

    @Override
    public int getItemCount() {
        return mListaPedido.size();
    }

    public class PedidoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mId;

        public PedidoViewHolder(@NonNull View itemView) {
            super(itemView);
            mId = itemView.findViewById(R.id.textView21);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mRecyclerViewOnClickListenerHack.onClickListener(view, getPosition());
        }
    }
}
