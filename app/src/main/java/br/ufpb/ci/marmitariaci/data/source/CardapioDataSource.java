package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;

public interface CardapioDataSource {

    interface LoadCardsCallback{
        void onCardsLoaded(List<Cardapio> cards);
        void onDataNotAvailable();
    }

    interface GetCardCallback{
        void onCardLoaded(Cardapio card);
        void onDataNotAvailable();
    }

    interface SaveCardCallback{
        void onCardStored();
        void onDataNotStored();
    }

    void getCards(boolean network, LoadCardsCallback callback);
    void getCard(boolean network, Integer id, GetCardCallback callback);
    void getCardFornecedor(Integer id, LoadCardsCallback callback);
    void saveCard(boolean network, Cardapio c, SaveCardCallback callback);
    void saveAllCard(List<Cardapio> cards, LoadCardsCallback callback);
    void refreshCards(LoadCardsCallback callback);
    void deleteAllCards(LoadCardsCallback callback);
    void deleteCard(boolean network, Integer id, SaveCardCallback callback);
}
