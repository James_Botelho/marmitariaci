package br.ufpb.ci.marmitariaci.data.source.local;

import android.os.AsyncTask;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.data.source.PedidoDataSource;

public class PedidoLocalDataSource implements PedidoDataSource {

    private static volatile PedidoLocalDataSource INSTANCE;

    private PedidoDAO pedidoDAO;

    private PedidoLocalDataSource(PedidoDAO dao){
        pedidoDAO = dao;
    }

    public static PedidoLocalDataSource getInstance(PedidoDAO pedidoDAO){
        if(INSTANCE == null){
            synchronized (PedidoLocalDataSource.class){
                if(INSTANCE == null){
                    INSTANCE = new PedidoLocalDataSource(pedidoDAO);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void getPedidos(boolean network, int id, final GetDataCallback callback) {
        new AsyncTask<Void, Void, List<Pedido>>(){
            @Override
            protected List<Pedido> doInBackground(Void... voids) {
                return pedidoDAO.getAll();
            }

            @Override
            protected void onPostExecute(List<Pedido> pedidos) {
                callback.onDataLoaded(pedidos);
            }
        }.execute();
    }

    @Override
    public void savePedido(boolean network, Pedido pedido, SavePedCallback callback) {

    }

    @Override
    public void deleteAll(GetDataCallback callback) {

    }

    @Override
    public void saveAll(final List<Pedido> pedidoList, final GetDataCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                pedidoDAO.insertAll(pedidoList);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onDataLoaded(null);
            }
        }.execute();
    }

    @Override
    public void deletePedido(int id, SavePedCallback callback) {

    }
}
