package br.ufpb.ci.marmitariaci.marmitadetalhe;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;
import br.ufpb.ci.marmitariaci.data.source.PedidoDataSource;

public class MarmitaDetalhePresenter implements MarmitaDetalheContract.Presenter, PedidoDataSource.SavePedCallback {

    private final PedidoDataSource mPedidoDataSource;
    private final LoginDataSource mLoginDataSource;

    private MarmitaDetalheContract.View mView;

    private Cardapio mCardapio;

    public MarmitaDetalhePresenter(PedidoDataSource pedidoDataSource, LoginDataSource loginDataSource, MarmitaDetalheContract.View view, Cardapio cardapio){
        mPedidoDataSource = pedidoDataSource;
        mLoginDataSource = loginDataSource;
        mView = view;
        mCardapio = cardapio;
        mView.setPresenter(this);
    }

    @Override
    public void realizaPedido(String... dados) {
        mView.exibeProgresso();
        Pedido pedido = new Pedido();
        pedido.setIdMarmita(mCardapio.getId());
        pedido.setItens(mCardapio.getItens());
        pedido.setIdFornecedor(mCardapio.getIdFornecedor());
        if(mLoginDataSource.getUserLogged().getCliente() != null) {
            pedido.setIdUsuario(mLoginDataSource.getUserLogged().getCliente().getId());
            mPedidoDataSource.savePedido(true, pedido, this);
        } else {
            mView.ocultaProgresso();
            mView.exibeMensagem("Você não é cliente", true);
        }
    }

    @Override
    public void start() {
        mView.setDados(mCardapio.getItens(), String.valueOf(mCardapio.getQtdItens()), mCardapio.getPreco());
    }

    @Override
    public void onDataStored() {
        mView.ocultaProgresso();
        mView.exibeMensagem("Pedido realizado", true);
    }

    @Override
    public void onDataNotStored() {
        mView.ocultaProgresso();
        mView.exibeMensagem("Falha ao realizar pedido", false);
    }
}
