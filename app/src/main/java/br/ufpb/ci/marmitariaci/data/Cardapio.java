package br.ufpb.ci.marmitariaci.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

@Entity
public class Cardapio implements Serializable {
    @PrimaryKey
    private int id;
    private String itens;
    private int qtdItens;
    private String data;
    private String preco;
    //Chave estrangeira da tabela de fornecedor, não especificado por motivos de concorrência
    private int idFornecedor;

    public Cardapio(){}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItens() {
        return itens;
    }

    public void setItens(String itens) {
        this.itens = itens;
    }

    public int getQtdItens() {
        return qtdItens;
    }

    public void setQtdItens(int qtdItens) {
        this.qtdItens = qtdItens;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    @Override
    public String toString() {
        return "Cardapio{" +
                "id=" + id +
                ", itens='" + itens + '\'' +
                ", qtdItens=" + qtdItens +
                ", data='" + data + '\'' +
                ", preco='" + preco + '\'' +
                ", id_fornecedor=" + idFornecedor +
                '}';
    }
}