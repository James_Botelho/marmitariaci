package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;

public interface FornecedorDataSource {

    //Carrega um conjunto de fornecedores
    interface LoadFornsCallback{
        void onFornsLoaded(List<Fornecedor> forns);
        void onDataNotAvailable();
    }

    //Carrega um unico fornecedor
    interface GetFornCallback{
        void onFornLoaded(Fornecedor forn);
        void onDataNotAvailable();
    }

    //Salva um unico fornecedor
    interface SaveFornCallback{
        void onFornStored(Fornecedor f);
        void onDataNotStored();
    }

    void getForns(boolean network, LoadFornsCallback callback);
    void getForn(boolean network, Integer id, GetFornCallback callback);
    void saveForn(boolean network, Fornecedor forn, SaveFornCallback callback);
    void updateForn(Fornecedor forn, SaveFornCallback callback);
    void saveAllForns(List<Fornecedor> forns, LoadFornsCallback callback);
    void refreshForns(LoadFornsCallback callback);
    void deleteAllForns(LoadFornsCallback callback);
    void deleteForn(boolean network, Integer id, GetFornCallback callback);
}
