package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface PerfilClienteContract {

    interface View extends BaseView<Presenter>{
        void setDadosPerfilCliente(String...dados);
    }

    interface Presenter extends BasePresenter{
        Context getContext();
    }
}
