package br.ufpb.ci.marmitariaci.data.source.local;

import android.os.AsyncTask;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.FornecedorDataSource;

public class FornecedorLocalDataSource implements FornecedorDataSource {

    private static volatile FornecedorLocalDataSource INSTANCE;

    private FornecedorDAO fornecedorDAO;

    private FornecedorLocalDataSource(FornecedorDAO dao){
        fornecedorDAO = dao;
    }

    public static FornecedorLocalDataSource getInstance(FornecedorDAO dao){
        if(INSTANCE == null){
            synchronized (FornecedorLocalDataSource.class){
                if(INSTANCE == null)
                    INSTANCE = new FornecedorLocalDataSource(dao);
            }
        }
        return INSTANCE;
    }

    @Override
    public void getForns(boolean network, final LoadFornsCallback callback) {
        new AsyncTask<Void, Void, List<Fornecedor>>(){
            @Override
            protected List<Fornecedor> doInBackground(Void... voids) {
                return fornecedorDAO.getAll();
            }

            @Override
            protected void onPostExecute(List<Fornecedor> fornecedors) {
                if(fornecedors.isEmpty()){
                    callback.onDataNotAvailable();
                } else {
                    callback.onFornsLoaded(fornecedors);
                }
            }
        }.execute();
    }

    @Override
    public void getForn(boolean network, final Integer id, final GetFornCallback callback) {
        new AsyncTask<Void, Void, Fornecedor>(){
            @Override
            protected Fornecedor doInBackground(Void... voids) {
                return fornecedorDAO.getBYId(id);
            }

            @Override
            protected void onPostExecute(Fornecedor fornecedor) {
                if(fornecedor == null)
                    callback.onDataNotAvailable();
                else
                    callback.onFornLoaded(fornecedor);
            }
        }.execute();
    }

    @Override
    public void saveForn(boolean network, Fornecedor forn, SaveFornCallback callback) {
        //Não é necessário implementar
    }

    @Override
    public void updateForn(Fornecedor forn, SaveFornCallback callback) {
        //Não é necessário implementar
    }

    @Override
    public void saveAllForns(final List<Fornecedor> forns, final LoadFornsCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                fornecedorDAO.insertAll(forns);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onFornsLoaded(null);
            }
        }.execute();
    }

    @Override
    public void refreshForns(LoadFornsCallback callback) {
        //Não é necessário implementar
    }

    @Override
    public void deleteAllForns(final LoadFornsCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                fornecedorDAO.deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onFornsLoaded(null);
            }
        }.execute();
    }

    @Override
    public void deleteForn(boolean network, Integer id, GetFornCallback callback) {

    }
}
