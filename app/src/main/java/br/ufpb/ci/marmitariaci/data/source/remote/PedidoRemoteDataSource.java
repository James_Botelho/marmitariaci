package br.ufpb.ci.marmitariaci.data.source.remote;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.data.source.PedidoDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.PedidoService;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.RestPed;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidoRemoteDataSource implements PedidoDataSource {

    private static volatile PedidoRemoteDataSource INSTANCE;

    private PedidoService pedidoService;

    private PedidoRemoteDataSource(){
        pedidoService = RestPed.getPedidoService();
    }

    public static PedidoRemoteDataSource getInstance(){
        if(INSTANCE == null)
            INSTANCE = new PedidoRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getPedidos(boolean network, int id, final GetDataCallback callback) {
        pedidoService.listaPedido(HttpUtil.getToken()).enqueue(new Callback<List<Pedido>>() {
            @Override
            public void onResponse(Call<List<Pedido>> call, Response<List<Pedido>> response) {
                if(response.code() == 200){
                    callback.onDataLoaded(response.body());
                }else{
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<List<Pedido>> call, Throwable t) {
                t.printStackTrace();
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void savePedido(boolean network, Pedido pedido, final SavePedCallback callback) {
        pedidoService.adicionaPedido(pedido, HttpUtil.getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200){
                    callback.onDataStored();
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void deleteAll(GetDataCallback callback) {

    }

    @Override
    public void saveAll(List<Pedido> pedidoList, GetDataCallback callback) {

    }

    @Override
    public void deletePedido(int id, final SavePedCallback callback) {
        pedidoService.deletaPedido(id, HttpUtil.getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200){
                    callback.onDataStored();
                }else{
                    callback.onDataStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataStored();
            }
        });
    }
}
