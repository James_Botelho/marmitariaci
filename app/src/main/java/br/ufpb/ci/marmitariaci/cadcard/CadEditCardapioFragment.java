package br.ufpb.ci.marmitariaci.cadcard;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CadEditCardapioFragment extends Fragment implements CadEditCardapioContract.View {

    private CadEditCardapioContract.Presenter mPresenter;

    private TextView mItens;
    private TextView mPreco;
    private Button mBotaoCadastrar;
    private ProgressDialog dialogoProgresso;

    public static CadEditCardapioFragment newInstance(){
        return new CadEditCardapioFragment();
    }

    public CadEditCardapioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cad_edit_cardapio, container, false);
        mItens = v.findViewById(R.id.editText);
        mPreco = v.findViewById(R.id.editText2);
        mBotaoCadastrar = v.findViewById(R.id.cadCardapioButton);
        mBotaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.salvar(mItens.getText().toString(), mPreco.getText().toString());
            }
        });
        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void exibeProgresso() {
        dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
        if(encerra)
            getActivity().finish();
    }

    @Override
    public void setPresenter(CadEditCardapioContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
