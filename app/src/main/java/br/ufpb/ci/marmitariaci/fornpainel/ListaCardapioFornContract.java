package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;
import br.ufpb.ci.marmitariaci.data.Cardapio;

public interface ListaCardapioFornContract {

    interface View extends BaseView<Presenter>{
        void setListaCardapio(List<Cardapio> listaCardapio);
        void iniciaActivity(Intent intent);
    }

    interface Presenter extends BasePresenter{
        Context getContext();
        void opcaoSelecionada(int i, Cardapio cardapio);
    }
}
