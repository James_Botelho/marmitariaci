package br.ufpb.ci.marmitariaci.fornpainel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFornFragment extends Fragment implements PerfilFornContract.View {

    private PerfilFornContract.Presenter mPresenter;
    private TextView mNomeEmpresa;
    private TextView mNomeLogin;
    private TextView mTelefone;
    private TextView mEndereco;


    public static PerfilFornFragment newInstance(){
        return new PerfilFornFragment();
    }

    public PerfilFornFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_perfil_forn, container, false);

        mNomeEmpresa = view.findViewById(R.id.nomePerfilTextId);
        mNomeLogin = view.findViewById(R.id.usuarioPerfilTextId);
        mTelefone =  view.findViewById(R.id.telefonePerfilTextId);
        mEndereco = view.findViewById(R.id.enderecoPerfilTextId);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setDados(String... dados) {
        mNomeEmpresa.setText(dados[0]);
        mNomeLogin.setText(dados[1]);
        mTelefone.setText(dados[2]);
        mEndereco.setText(dados[3]);
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(PerfilFornContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
