package br.ufpb.ci.marmitariaci.data.source;

import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Login;

public interface LoginDataSource {
    interface LoginUserCallback{
        void onUserLogged(Login login);
        void onUserNotLogged(int codigo);
    }

    void loginUser(Credencial credencial, LoginUserCallback callback);
    void saveUserLogged(Login login);
    Login getUserLogged();
    void logoutUser();
}
