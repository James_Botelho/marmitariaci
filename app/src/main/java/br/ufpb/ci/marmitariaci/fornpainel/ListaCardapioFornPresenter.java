package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.CardapioDataSource;
import br.ufpb.ci.marmitariaci.data.source.FornecedorDataSource;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;
import br.ufpb.ci.marmitariaci.marmitadetalhe.MarmitaDetalheActivity;

public class ListaCardapioFornPresenter implements ListaCardapioFornContract.Presenter, CardapioDataSource.LoadCardsCallback, CardapioDataSource.SaveCardCallback {

    private final CardapioDataSource mCardapioDataSource;
    private final LoginDataSource mLoginDataSource;
    private final ListaCardapioFornContract.View mView;
    private boolean mNetwork;
    private static boolean faltaAcessar;
    private static List<Cardapio> listaCardapioTemp;

    public ListaCardapioFornPresenter(CardapioDataSource cardapioDataSource, LoginDataSource loginDataSource, ListaCardapioFornContract.View view, boolean network){
        this.mCardapioDataSource = cardapioDataSource;
        this.mLoginDataSource = loginDataSource;
        this.mView = view;
        this.mNetwork = network;
        view.setPresenter(this);
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getActivity();
    }

    @Override
    public void opcaoSelecionada(int i, Cardapio cardapio) {
        if(i == 0){
            Intent intent = new Intent(getContext(), MarmitaDetalheActivity.class);
            intent.putExtra("cardapio", cardapio);
            mView.iniciaActivity(intent);
        }else{
            mCardapioDataSource.deleteCard(true, cardapio.getId(), this);
        }
    }

    @Override
    public void start() {
        if(mNetwork){
            mCardapioDataSource.getCardFornecedor(mLoginDataSource.getUserLogged().getFornecedor().getId(), this);
        }else {
            mCardapioDataSource.getCards(mNetwork, this);
        }
    }

    @Override
    public void onCardsLoaded(List<Cardapio> cards) {
        if(mNetwork){
            faltaAcessar = true;
            listaCardapioTemp = cards;
            mCardapioDataSource.deleteAllCards(this);
            mNetwork = false;
        }else if(faltaAcessar){
            mCardapioDataSource.saveAllCard(listaCardapioTemp, this);
            faltaAcessar = false;
        } else {
            if(listaCardapioTemp != null)
                mView.setListaCardapio(listaCardapioTemp);
            else
                mView.setListaCardapio(cards);
        }
    }

    @Override
    public void onDataNotAvailable() {
        mView.exibeMensagem("Não há cardápios a serem exibidos", false);
    }

    @Override
    public void onCardStored() {
        mView.exibeMensagem("Cardapio deletado com sucesso", false);
    }

    @Override
    public void onDataNotStored() {
        mView.exibeMensagem("Erro ao deletar cardapio", false);
    }
}
