package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.local.FornecedorLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.FornecedorRemoteDataSource;

public class FornecedorRepository implements FornecedorDataSource {
    private static FornecedorRepository INSTANCE = null;
    private final FornecedorDataSource mFornecedorRemoteDataSource;
    private final FornecedorDataSource mFornecedorLocalDataSource;

    private FornecedorRepository(FornecedorDataSource remote, FornecedorDataSource local){
        mFornecedorRemoteDataSource = remote;
        mFornecedorLocalDataSource = local;
    }

    public static FornecedorRepository getInstance(FornecedorRemoteDataSource remote, FornecedorLocalDataSource local){
        if(INSTANCE == null)
            INSTANCE = new FornecedorRepository(remote, local);
        return INSTANCE;
    }

    @Override
    public void getForns(boolean network, LoadFornsCallback callback) {
        if(network)
            mFornecedorRemoteDataSource.getForns(network, callback);
        else
            mFornecedorLocalDataSource.getForns(network, callback);
    }

    @Override
    public void getForn(boolean network, Integer id, GetFornCallback callback) {
        mFornecedorLocalDataSource.getForn(network, id, callback);
    }

    @Override
    public void saveForn(boolean network, Fornecedor forn, SaveFornCallback callback) {
        mFornecedorRemoteDataSource.saveForn(network, forn, callback);
    }

    @Override
    public void updateForn(Fornecedor forn, SaveFornCallback callback) {
        mFornecedorRemoteDataSource.updateForn(forn, callback);
    }

    @Override
    public void saveAllForns(List<Fornecedor> forns, LoadFornsCallback callback) {
        mFornecedorLocalDataSource.saveAllForns(forns, callback);
    }

    @Override
    public void refreshForns(LoadFornsCallback callback) {
        mFornecedorLocalDataSource.deleteAllForns(callback);
    }

    @Override
    public void deleteAllForns(LoadFornsCallback callback) {
        mFornecedorLocalDataSource.deleteAllForns(callback);
    }

    @Override
    public void deleteForn(boolean network, Integer id, GetFornCallback callback) {
        mFornecedorRemoteDataSource.deleteForn(network, id, callback);
    }
}
