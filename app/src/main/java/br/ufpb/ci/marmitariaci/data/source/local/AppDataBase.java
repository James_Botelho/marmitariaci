package br.ufpb.ci.marmitariaci.data.source.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.utils.ConverteData;

@Database(entities = {Cardapio.class, Fornecedor.class, Pedido.class, Cliente.class}, version = 1)
@TypeConverters(ConverteData.class)
public abstract class AppDataBase extends RoomDatabase {

    private static AppDataBase INSTANCE;

    public abstract ClienteDAO clienteDAO();
    public abstract FornecedorDAO fornecedorDAO();
    public abstract CardapioDAO cardapioDAO();
    public abstract PedidoDAO pedidoDAO();

    public static AppDataBase getInstance(Context context){
        if(INSTANCE == null)
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, "Appdb.db").build();
        return INSTANCE;
    }
}
