package br.ufpb.ci.marmitariaci.data.source.remote;

import android.util.Log;

import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Login;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.LoginService;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.RestLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRemoteDataSource implements LoginDataSource {

    private static volatile LoginRemoteDataSource INSTANCE;

    private LoginService service;

    private LoginRemoteDataSource(){
        service = RestLogin.getLoginService();
    }

    public static LoginRemoteDataSource getInstance() {
        if(INSTANCE == null)
            INSTANCE = new LoginRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void loginUser(Credencial credencial, final LoginUserCallback callback) {
        Log.i("Login", credencial.getLogin() + "|" + credencial.getSenha());
        service.logaUsuario(credencial).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if(response.code() == 200)
                    callback.onUserLogged(response.body());
                else
                    callback.onUserNotLogged(response.code());
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                callback.onUserNotLogged(404);
            }
        });
    }

    @Override
    public void saveUserLogged(Login login) {
        //Não é necessário implementar
    }

    @Override
    public Login getUserLogged() {
        //Não é necessário implementar
        return null;
    }

    @Override
    public void logoutUser() {
        //Não é necessário implementar
    }
}
