package br.ufpb.ci.marmitariaci.data.source.remote;

import android.util.Log;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.FornecedorDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.FornecedorService;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.RestForn;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FornecedorRemoteDataSource implements FornecedorDataSource{
    private static volatile FornecedorRemoteDataSource INSTANCE;

    private FornecedorService fornecedorService;

    private FornecedorRemoteDataSource(){
        fornecedorService = RestForn.getService();
    }

    public static FornecedorRemoteDataSource getInstance(){
        if(INSTANCE == null)
            INSTANCE = new FornecedorRemoteDataSource();
        return INSTANCE;
    }


    @Override
    public void getForns(boolean network, final LoadFornsCallback callback) {
        fornecedorService.listaFornecedorGeral(HttpUtil.getToken()).enqueue(new Callback<List<Fornecedor>>() {
            @Override
            public void onResponse(Call<List<Fornecedor>> call, Response<List<Fornecedor>> response) {
                Log.i("RESPONDEU 200", String.valueOf(response.code()));
                if(response.code() == 200) {
                    callback.onFornsLoaded(response.body());
                }else if(response.code() == 401)
                    callback.onDataNotAvailable();
                else
                    callback.onDataNotAvailable();
            }

            @Override
            public void onFailure(Call<List<Fornecedor>> call, Throwable t) {
                Log.i("Erro deu ruim", "a o");
                t.printStackTrace();
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getForn(boolean network, Integer id, GetFornCallback callback) {
        //Não é necessário implementar, essa informação é local
    }

    @Override
    public void saveForn(boolean network, Fornecedor forn, final SaveFornCallback callback) {
        fornecedorService.adicionaFornecedor(forn).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200){
                    callback.onFornStored(null);
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void updateForn(Fornecedor forn, final SaveFornCallback callback) {
        fornecedorService.atualizaFornecedor(HttpUtil.getToken(), forn, forn.getId()).enqueue(new Callback<Fornecedor>() {
            @Override
            public void onResponse(Call<Fornecedor> call, Response<Fornecedor> response) {
                if(response.code() == 200){
                    callback.onFornStored(null);
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Fornecedor> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void saveAllForns(List<Fornecedor> forns, LoadFornsCallback callback) {
        //Não é necessário implementar, essa informação é para cache local
    }

    @Override
    public void refreshForns(LoadFornsCallback callback) {
        //Não precisa implementar
    }

    @Override
    public void deleteAllForns(LoadFornsCallback callback) {
        //Não precisa implementar, essa informação é local
    }

    @Override
    public void deleteForn(boolean network, Integer id, final GetFornCallback callback) {
        fornecedorService.deletaFornecedor(HttpUtil.getToken(), id).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200)
                    callback.onFornLoaded(null);
                else if(response.code() == 401)
                    callback.onDataNotAvailable();
                else
                    callback.onDataNotAvailable();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataNotAvailable();
            }
        });
    }
}
