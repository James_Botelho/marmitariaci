package br.ufpb.ci.marmitariaci;

public interface BasePresenter {
    void start();
}
