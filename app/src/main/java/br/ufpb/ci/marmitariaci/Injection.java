package br.ufpb.ci.marmitariaci;

import android.content.Context;

import br.ufpb.ci.marmitariaci.data.source.CardapioRepository;
import br.ufpb.ci.marmitariaci.data.source.ClienteRepository;
import br.ufpb.ci.marmitariaci.data.source.FornecedorRepository;
import br.ufpb.ci.marmitariaci.data.source.LoginRepository;
import br.ufpb.ci.marmitariaci.data.source.PedidoRepository;
import br.ufpb.ci.marmitariaci.data.source.local.AppDataBase;
import br.ufpb.ci.marmitariaci.data.source.local.CardapioLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.local.ClienteLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.local.FornecedorLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.local.LoginLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.local.PedidoLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.CardapioRemoteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.ClienteRemoteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.FornecedorRemoteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.LoginRemoteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.PedidoRemoteDataSource;

public class Injection {

    public static FornecedorRepository getFornecedorRepository(Context context){

        AppDataBase appDataBase = AppDataBase.getInstance(context);

        FornecedorLocalDataSource local = FornecedorLocalDataSource.getInstance(appDataBase.fornecedorDAO());
        FornecedorRemoteDataSource remoto = FornecedorRemoteDataSource.getInstance();
        return FornecedorRepository.getInstance(remoto, local);
    }

    public static CardapioRepository getCardapioRepository(Context context){

        AppDataBase appDataBase = AppDataBase.getInstance(context);

        CardapioLocalDataSource local = CardapioLocalDataSource.getInstance(appDataBase.cardapioDAO());
        CardapioRemoteDataSource remoto = CardapioRemoteDataSource.getInstance();
        return CardapioRepository.getInstance(local, remoto);
    }

    public static ClienteRepository getClienteRepository(Context context){
        AppDataBase appDataBase = AppDataBase.getInstance(context);
        ClienteLocalDataSource local = ClienteLocalDataSource.getInstance(appDataBase.clienteDAO());
        ClienteRemoteDataSource remoto = ClienteRemoteDataSource.getInstance();

        return ClienteRepository.getInstance(local, remoto);
    }

    public static LoginRepository getLoginRepository(Context context){
        LoginLocalDataSource local = LoginLocalDataSource.getInstance(context);
        LoginRemoteDataSource remoto = LoginRemoteDataSource.getInstance();

        return LoginRepository.getInstance(local, remoto);
    }

    public static PedidoRepository getPedidoRepository(Context context){
        AppDataBase appDataBase = AppDataBase.getInstance(context);
        PedidoLocalDataSource localDataSource = PedidoLocalDataSource.getInstance(appDataBase.pedidoDAO());
        PedidoRemoteDataSource remoteDataSource = PedidoRemoteDataSource.getInstance();

        return PedidoRepository.getInstance(remoteDataSource, localDataSource);
    }
}
