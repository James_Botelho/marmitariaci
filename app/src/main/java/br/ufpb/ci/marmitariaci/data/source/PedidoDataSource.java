package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;

public interface PedidoDataSource {
    interface SavePedCallback{
        void onDataStored();
        void onDataNotStored();
    }

    interface GetDataCallback{
        void onDataLoaded(List<Pedido> pedidos);
        void onDataNotAvailable();
    }

    void getPedidos(boolean network, int id, GetDataCallback callback);
    void savePedido(boolean network, Pedido pedido, SavePedCallback callback);
    void saveAll(List<Pedido> pedidoList, GetDataCallback callback);
    void deleteAll(GetDataCallback callback);
    void deletePedido(int id, SavePedCallback callback);
}
