package br.ufpb.ci.marmitariaci;

import android.view.View;

public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
