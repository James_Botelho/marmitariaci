package br.ufpb.ci.marmitariaci.cadeditforn;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.ufpb.ci.marmitariaci.Injection;
import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.utils.ActivityUtil;

public class CadEditFornecedor extends AppCompatActivity {

    private CadEditFornecedorPresenter mCadEditFornecedorPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_edit_fornecedor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CadEditFornecedorFragment cadEditFornecedorFragment = (CadEditFornecedorFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        Fornecedor fornecedor = (Fornecedor) getIntent().getSerializableExtra(CadEditFornecedorFragment.ARGUMENTO_FORNECEDOR);

        if(cadEditFornecedorFragment == null){
            cadEditFornecedorFragment = new CadEditFornecedorFragment();

            if(getIntent().hasExtra(CadEditFornecedorFragment.ARGUMENTO_FORNECEDOR)){
                Bundle bundle = new Bundle();
                bundle.putSerializable(CadEditFornecedorFragment.ARGUMENTO_FORNECEDOR, fornecedor);
                cadEditFornecedorFragment.setArguments(bundle);
            }

            ActivityUtil.addFragmentToActivity(getSupportFragmentManager(), cadEditFornecedorFragment, R.id.contentFrame);
        }

        mCadEditFornecedorPresenter = new CadEditFornecedorPresenter(cadEditFornecedorFragment, Injection.getFornecedorRepository(getApplicationContext()), fornecedor);
    }
}
