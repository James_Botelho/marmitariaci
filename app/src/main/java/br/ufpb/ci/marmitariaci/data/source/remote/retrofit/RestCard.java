package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestCard {
    private static CardapioService service = null;

    private static CardapioService initCardService(){
        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(HttpUtil.getURL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(CardapioService.class);
    }

    public static CardapioService getService(){
        if(service == null)
            service = initCardService();
        return service;
    }
}
