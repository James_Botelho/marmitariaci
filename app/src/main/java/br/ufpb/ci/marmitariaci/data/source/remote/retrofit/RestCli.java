package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestCli {
    private static ClienteService service = null;

    private static ClienteService initFornService(){
        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(HttpUtil.getURL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ClienteService.class);
    }

    public static ClienteService getService(){
        if(service == null)
            service = initFornService();
        return service;
    }
}
