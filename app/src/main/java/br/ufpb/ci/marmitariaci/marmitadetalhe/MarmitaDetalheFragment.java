package br.ufpb.ci.marmitariaci.marmitadetalhe;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MarmitaDetalheFragment extends Fragment implements MarmitaDetalheContract.View {


    private MarmitaDetalheContract.Presenter mPresenter;

    private TextView mItens;
    private TextView mQtd;
    private TextView mPreco;
    private Button mBotaoPedido;

    public MarmitaDetalheFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_marmita_detalhe, container, false);

        mItens = view.findViewById(R.id.itensDetalhasMarmitasId);
        mQtd = view.findViewById(R.id.qtdItensMarmitaDetalhesId);
        mPreco = view.findViewById(R.id.precoDetalhesMarmitaId);
        mBotaoPedido = view.findViewById(R.id.button);
        mBotaoPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.realizaPedido();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setDados(String... dados) {
        mItens.setText(dados[0]);
        mQtd.setText(dados[1]);
        mPreco.setText(dados[2]);
    }

    @Override
    public void exibeProgresso() {

    }

    @Override
    public void ocultaProgresso() {

    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
        if(encerra)
            getActivity().finish();
    }

    @Override
    public void setPresenter(MarmitaDetalheContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
