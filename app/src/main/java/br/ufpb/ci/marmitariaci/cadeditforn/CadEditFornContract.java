package br.ufpb.ci.marmitariaci.cadeditforn;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface CadEditFornContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
        void setEndereco(String endereco);
        void setTextNomeEmpresa(String nomeEmpresa);
        void setTextUsuario(String usuario);
        void setTextTelefone(String telefone);
        void setTextHintSenha(String senha);
        void setTextButton(String texto);
    }

    interface Presenter extends BasePresenter{
        void salva(String... strings);
        Context getContext();
    }
}
