package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.ufpb.ci.marmitariaci.Injection;

public class TabClientAdapter extends FragmentStatePagerAdapter {

    private ListaCardapioPresenter mListaCardapioPresenter;
    private ListaFornecedorPresenter mListaFornecedorPresenter;
    private PerfilClientePresenter mPerfilClientePresenter;
    private String mNomeTabs[];
    private Context mContext;
    private boolean mNetwork;
    private static int controleNetwork;

    public TabClientAdapter(FragmentManager fm, String[] nomeTabs, Context context, boolean network) {
        super(fm);
        this.mNomeTabs = nomeTabs;
        this.mContext = context;
        this.mNetwork = network;
        controleNetwork = 0;
    }

    private synchronized void contadorControleNetwork(){
        controleNetwork++;
        if(controleNetwork >= 3)
            this.mNetwork = false;
    }

    @Override
    public Fragment getItem(int position) {
        if(position != 2)
            contadorControleNetwork();
        switch (position){
            case 0:
                ListaCardapioFragment listaCardapioFragment = ListaCardapioFragment.newInstance();
                mListaCardapioPresenter = new ListaCardapioPresenter(Injection.getCardapioRepository(mContext), listaCardapioFragment, mNetwork);
                return listaCardapioFragment;
            case 1:
                ListFornFragment listFornFragment = ListFornFragment.newInstance();
                mListaFornecedorPresenter = new ListaFornecedorPresenter(Injection.getFornecedorRepository(mContext), listFornFragment, mNetwork);
                return listFornFragment;
            case 2:
                PerfilClienteFragment perfilClienteFragment = PerfilClienteFragment.newInstance();
                mPerfilClientePresenter = new PerfilClientePresenter( perfilClienteFragment, Injection.getLoginRepository(mContext));
                return perfilClienteFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNomeTabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mNomeTabs[position];
    }
}
