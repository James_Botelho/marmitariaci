package br.ufpb.ci.marmitariaci.marmitadetalhe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.ufpb.ci.marmitariaci.Injection;
import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.utils.ActivityUtil;

public class MarmitaDetalheActivity extends AppCompatActivity {

    private MarmitaDetalhePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marmita_detalhe);
        Cardapio cardapio = (Cardapio) getIntent().getSerializableExtra("cardapio");

        MarmitaDetalheFragment marmitaDetalheFragment = (MarmitaDetalheFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if(marmitaDetalheFragment == null){
            marmitaDetalheFragment = new MarmitaDetalheFragment();
        }

        ActivityUtil.addFragmentToActivity(getSupportFragmentManager(), marmitaDetalheFragment, R.id.contentFrame);

        mPresenter = new MarmitaDetalhePresenter(Injection.getPedidoRepository(getApplicationContext()), Injection.getLoginRepository(getApplicationContext()), marmitaDetalheFragment, cardapio);


    }
}
