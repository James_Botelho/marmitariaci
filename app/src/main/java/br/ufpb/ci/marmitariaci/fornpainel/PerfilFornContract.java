package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface PerfilFornContract {

    interface View extends BaseView<Presenter>{
        void setDados(String...dados);
    }

    interface Presenter extends BasePresenter{
        Context getContext();
    }
}
