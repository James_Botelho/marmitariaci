package br.ufpb.ci.marmitariaci.data.source.remote;

import android.util.Log;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.CardapioDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.CardapioService;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.RestCard;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardapioRemoteDataSource implements CardapioDataSource {
    private static volatile CardapioRemoteDataSource INSTANCE;

    private CardapioService cardapioService;

    private CardapioRemoteDataSource(){
        cardapioService = RestCard.getService();
    }

    public static CardapioRemoteDataSource getInstance(){
        if(INSTANCE == null)
            INSTANCE = new CardapioRemoteDataSource();
        return INSTANCE;
    }


    @Override
    public void getCards(boolean network, final LoadCardsCallback callback) {
        Log.i("getCardsNetwork", "getCardsNetwork");
        cardapioService.listaCardapioGeral(HttpUtil.getToken()).enqueue(new Callback<List<Cardapio>>() {
            @Override
            public void onResponse(Call<List<Cardapio>> call, Response<List<Cardapio>> response) {
                if(response.code() == 200) {
                    for(Cardapio c: response.body()){
                        Log.i("Cardapio", c.getItens());
                    }
                    callback.onCardsLoaded(response.body());
                }else if(response.code() == 401) {
                    callback.onDataNotAvailable();
                }else {
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<List<Cardapio>> call, Throwable t) {
                t.printStackTrace();
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getCard(boolean network, Integer id, GetCardCallback callback) {

    }

    @Override
    public void getCardFornecedor(Integer id, final LoadCardsCallback callback) {
        Log.i("Token" , HttpUtil.getToken());
        cardapioService.listaCardapioForn(id, HttpUtil.getToken()).enqueue(new Callback<List<Cardapio>>() {
            @Override
            public void onResponse(Call<List<Cardapio>> call, Response<List<Cardapio>> response) {
                if(response.code() == 200){
                    callback.onCardsLoaded(response.body());
                }else{
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<List<Cardapio>> call, Throwable t) {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveCard(boolean network, Cardapio c, final SaveCardCallback callback) {
        cardapioService.insereCardapio(c, HttpUtil.getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("Resposta", String.valueOf(response.code()));
                if(response.code() == 200){
                    callback.onCardStored();
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void saveAllCard(List<Cardapio> cards, LoadCardsCallback callback) {
        //Não é necessário implementar
    }

    @Override
    public void refreshCards(LoadCardsCallback callback) {

    }

    @Override
    public void deleteAllCards(LoadCardsCallback callback) {

    }

    @Override
    public void deleteCard(boolean network, Integer id, final SaveCardCallback callback) {
        cardapioService.deletaCardapio(id, HttpUtil.getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("Resposta", String.valueOf(response.code()));
                if(response.code() == 200) {
                    callback.onCardStored();
                }else {
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                callback.onDataNotStored();
            }
        });
    }
}
