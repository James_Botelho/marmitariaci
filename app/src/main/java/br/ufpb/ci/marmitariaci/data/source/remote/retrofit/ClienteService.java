package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cliente;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ClienteService {
    @GET("cliente")
    Call<List<Cliente>> listaClientes(@Header("Authorization") String jwt);

    @POST("cliente")
    Call<Void> adicionaCliente(@Body Cliente cliente);

    @PUT("cliente/{id}")
    Call<Cliente> atualizaCliente(@Header("Authorization") String jwt, @Body Cliente cliente, @Path("id") int id);

    @DELETE("cliente/{id}")
    Call<Void> deletaCliente(@Header("Authorization") String jwt, @Path("id") int id);
}
