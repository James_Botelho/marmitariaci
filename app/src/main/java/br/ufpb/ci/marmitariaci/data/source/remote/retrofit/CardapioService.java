package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CardapioService {
    @GET("marmitas") //Cardapios de todos os fornecedores
    Call<List<Cardapio>> listaCardapioGeral(@Header("Authorization") String jwt);

    @GET("marmitas/{id}") //Cardapios pelo id do fornecedor
    Call<List<Cardapio>> listaCardapioForn(@Path("id") int id, @Header("Authorization") String jwt);

    @POST("marmitas") //Insere um cardapio
    Call<Void> insereCardapio(@Body Cardapio c, @Header("Authorization") String jwt);

    @PUT("marmitas/{id}") //Edita um cardapio
    Call<Cardapio> atualizaCardapio(@Body Cardapio c, @Path("id") int id, @Header("Authorization") String jwt);

    @DELETE("marmitas/{id}") //Deleta um cardapio pelo seu id
    Call<Void> deletaCardapio(@Path("id") int id, @Header("Authorization") String jwt);
}
