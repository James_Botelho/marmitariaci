package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.Fornecedor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FornecedorService {
    @GET("fornecedor")
    Call<List<Fornecedor>> listaFornecedorGeral(@Header("Authorization") String jwt);

    @POST("fornecedor")
    Call<Void> adicionaFornecedor(@Body Fornecedor f);

    @PUT("fornecedor/{id}")
    Call<Fornecedor> atualizaFornecedor(@Header("Authorization") String jwt, @Body Fornecedor f, @Path("id") int id);

    @DELETE("fornecedor/{id}")
    Call<Void> deletaFornecedor(@Header("Authorization") String jwt, @Path("id") int id);
}
