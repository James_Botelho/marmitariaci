package br.ufpb.ci.marmitariaci.data.source;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.source.local.ClienteLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.ClienteRemoteDataSource;

public class ClienteRepository implements ClienteDataSource {

    private static ClienteRepository INSTANCE = null;
    private final ClienteRemoteDataSource mClienteRemoteDataSource;
    private final ClienteLocalDataSource mClienteLocalDataSource;

    private ClienteRepository(ClienteLocalDataSource local, ClienteRemoteDataSource remoto){
        mClienteLocalDataSource = local;
        mClienteRemoteDataSource = remoto;
    }

    public static ClienteRepository getInstance(ClienteLocalDataSource local, ClienteRemoteDataSource remoto){
        if(INSTANCE == null)
            INSTANCE = new ClienteRepository(local, remoto);
        return INSTANCE;
    }

    @Override
    public void getClients(LoadClientsCallback callback) {
        mClienteRemoteDataSource.getClients(callback);
    }

    @Override
    public void getClient(Integer id, GetClientCallback callback) {
        mClienteLocalDataSource.getClient(id, callback);
    }

    @Override
    public void saveClient(Cliente c, SaveClientCallback callback) {
        mClienteRemoteDataSource.saveClient(c, callback);
    }

    @Override
    public void updateClient(Cliente c, SaveClientCallback callback) {
        mClienteRemoteDataSource.saveClient(c, callback);
    }

    @Override
    public void refreshClients(LoadClientsCallback callback) {

    }

    @Override
    public void deleteAllClients(LoadClientsCallback callback) {
        mClienteLocalDataSource.deleteAllClients(callback);
    }

    @Override
    public void deleteClient(Integer id, SaveClientCallback callback) {
        mClienteRemoteDataSource.deleteClient(id, callback);
    }
}
