package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;
import br.ufpb.ci.marmitariaci.data.Cardapio;

public class CardapioAdapter extends RecyclerView.Adapter<CardapioAdapter.CardapioViewHolder> {

    private List<Cardapio> mListaCardapio;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public CardapioAdapter(Context context, List<Cardapio> listaCardapio) {
        this.mListaCardapio = listaCardapio;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }

    @NonNull
    @Override
    public CardapioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_cardapio, parent,false);
        CardapioViewHolder cardapioViewHolder = new CardapioViewHolder(view);
        return cardapioViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CardapioViewHolder holder, int position) {
        String mensagemTemplate = mContext.getString(R.string.preco_lista_label);
        String valorLista = mListaCardapio.get(position).getPreco();
        holder.mTituloPrecoId.setText(String.format(mensagemTemplate, valorLista));

        mensagemTemplate = mContext.getString(R.string.qtd_itens_label);
        valorLista = String.valueOf(mListaCardapio.get(position).getQtdItens());
        holder.mTituloQtdId.setText(String.format(mensagemTemplate, valorLista));
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public Cardapio getItem(int position){
        return mListaCardapio.get(position);
    }

    @Override
    public int getItemCount() {
        return mListaCardapio.size();
    }

    public class CardapioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTituloPrecoId;
        public TextView mTituloQtdId;

        public CardapioViewHolder(@NonNull View itemView) {
            super(itemView);

            mTituloPrecoId = itemView.findViewById(R.id.tituloPrecoId);
            mTituloQtdId = itemView.findViewById(R.id.tituloQtdItensId);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mRecyclerViewOnClickListenerHack.onClickListener(view, getPosition());
        }
    }
}
