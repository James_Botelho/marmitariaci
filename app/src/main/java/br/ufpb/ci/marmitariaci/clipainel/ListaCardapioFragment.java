package br.ufpb.ci.marmitariaci.clipainel;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;
import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.marmitadetalhe.MarmitaDetalheActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaCardapioFragment extends Fragment implements ListaCardapioContract.View, RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ListaCardapioContract.Presenter mPresenter;
    private ProgressDialog dialogoProgresso;

    public static ListaCardapioFragment newInstance(){
        return new ListaCardapioFragment();
    }

    public ListaCardapioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cardapio, container, false);

        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);

        mRecyclerView = view.findViewById(R.id.rv_cardapio);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(llm);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void exibeProgresso() {
        //dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void setListaCardapio(List<Cardapio> listaCardapio) {
        CardapioAdapter adapter = new CardapioAdapter(getActivity(), listaCardapio);
        adapter.setRecyclerViewOnClickListenerHack(this);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(ListaCardapioContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public boolean estaAtiva() {
        return isAdded();
    }

    @Override
    public void onClickListener(View view, int position) {
        CardapioAdapter adapter = (CardapioAdapter) mRecyclerView.getAdapter();
        Cardapio c = adapter.getItem(position);
        Intent intent = new Intent(getActivity(), MarmitaDetalheActivity.class);
        intent.putExtra("cardapio", c);
        startActivity(intent);
    }
}
