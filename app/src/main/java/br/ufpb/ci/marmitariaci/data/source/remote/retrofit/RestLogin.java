package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestLogin {

    private static LoginService service = null;

    private static LoginService initLoginService(){
        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(HttpUtil.getURL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(LoginService.class);
    }

    public static LoginService getLoginService(){
        if(service == null)
            service = initLoginService();
        return service;
    }
}
