package br.ufpb.ci.marmitariaci.clipainel;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Fornecedor;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFornFragment extends Fragment implements ListaFornecedorContract.View {

    private RecyclerView mRecyclerViewForn;
    private ListaFornecedorContract.Presenter mPresenter;
    private ProgressDialog dialogoProgresso;

    public static ListFornFragment newInstance(){
        return new ListFornFragment();
    }

    public ListFornFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_forn, container, false);

        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);

        mRecyclerViewForn = view.findViewById(R.id.rv_forn);
        mRecyclerViewForn.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewForn.setLayoutManager(llm);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void exibeProgresso() {
        //dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
        FornecedorAdapter adapter = new FornecedorAdapter(getActivity(), listaFornecedor);
        mRecyclerViewForn.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean estaAtiva() {
        return isAdded();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(ListaFornecedorContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
