package br.ufpb.ci.marmitariaci.utils;

import android.content.Context;
import android.util.Log;

import br.ufpb.ci.marmitariaci.R;

public class HttpUtil {
    private static final String URL = "http://192.168.0.9:8080/rest/";
    private static String token_auth;

    public static String getURL() {
        return URL;
    }

    public static void setToken(String token){
        token_auth = "Bearer " + token;
    }

    public static String getToken(){
        Log.i("Authorization", token_auth);
        return token_auth;
    }

    public static String getMensagem(Context context, int codigo){
        if(codigo == 401)
            return context.getString(R.string.mensagem_login_falha);

        Log.i("HttpUtil", String.valueOf(codigo));
        return context.getString(R.string.sem_conexao);
    }
}
