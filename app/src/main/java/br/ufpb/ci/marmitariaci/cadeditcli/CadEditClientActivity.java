package br.ufpb.ci.marmitariaci.cadeditcli;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import br.ufpb.ci.marmitariaci.Injection;
import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.utils.ActivityUtil;

public class CadEditClientActivity extends AppCompatActivity {


    public static final  int REQUEST_ADD_TASK = 1;

    private CadEditCliPresenter mCadEditCliPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_edit_client);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CadEditClientFragment cadEditClientFragment = (CadEditClientFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        String cliId = getIntent().getStringExtra(CadEditClientFragment.ARGUMENT_EDIT_CLIENT_ID);
        Integer idCliente;
        if(cliId == null){
            idCliente = null;
        }else{
            idCliente = Integer.parseInt(cliId);
        }

        if(cadEditClientFragment == null){
            cadEditClientFragment = CadEditClientFragment.newInstance();

            if(getIntent().hasExtra(CadEditClientFragment.ARGUMENT_EDIT_CLIENT_ID)){
                Bundle bundle = new Bundle();
                bundle.putString(CadEditClientFragment.ARGUMENT_EDIT_CLIENT_ID, cliId);
                cadEditClientFragment.setArguments(bundle);
            }

            ActivityUtil.addFragmentToActivity(getSupportFragmentManager(), cadEditClientFragment, R.id.contentFrame);
        }
        mCadEditCliPresenter = new CadEditCliPresenter(idCliente, Injection.getClienteRepository(getApplicationContext()),cadEditClientFragment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
