package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestPed {
    private static PedidoService service = null;

    private static PedidoService initPedidoService(){
        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(HttpUtil.getURL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(PedidoService.class);
    }

    public static PedidoService getPedidoService(){
        if(service == null)
            service = initPedidoService();
        return service;
    }
}
