package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.cadcard.CadEditCardapioActivity;
import br.ufpb.ci.marmitariaci.clipainel.TabClientAdapter;
import br.ufpb.ci.marmitariaci.utils.SlidingTabLayout;

public class PainelFornecedorActivity extends AppCompatActivity {
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private TabFornAdapter adapter;
    private int tabActived;

    private void iniciaFragment(boolean network){
        adapter = new TabFornAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.nome_tabs_fornecedor), getApplicationContext(), network);
        viewPager.setAdapter(adapter);
        if(tabActived != -1)
            viewPager.setCurrentItem(tabActived);
        slidingTabLayout.setViewPager(viewPager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painel_cli);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        slidingTabLayout = findViewById(R.id.sld_layout_cliente);
        viewPager = findViewById(R.id.vp_cliente);
        tabActived = -1;
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.colorAccent));
        iniciaFragment(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_painel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.atualizar:
                tabActived = viewPager.getCurrentItem();
                iniciaFragment(true);
                break;
            case R.id.add_marmita:
                startActivity(new Intent(this, CadEditCardapioActivity.class));
                break;
        }
        return true;
    }
}
