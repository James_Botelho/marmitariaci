package br.ufpb.ci.marmitariaci.cadcard;

import android.content.Context;
import android.support.v4.app.Fragment;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.CardapioDataSource;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;

public class CadEditCardapioPresenter implements CadEditCardapioContract.Presenter, CardapioDataSource.SaveCardCallback {
    private final CardapioDataSource mCardapioDataSource;
    private final LoginDataSource mLoginDataSource;
    private final CadEditCardapioContract.View mView;

    public CadEditCardapioPresenter(CadEditCardapioContract.View view, CardapioDataSource cardapioDataSource, LoginDataSource loginDataSource){
        this.mView = view;
        this.mCardapioDataSource = cardapioDataSource;
        this.mLoginDataSource = loginDataSource;
        mView.setPresenter(this);
    }

    @Override
    public void salvar(String... strings) {
        mView.exibeProgresso();
        String itens = strings[0];
        String preco = strings[1];
        Cardapio cardapio = new Cardapio();
        cardapio.setItens(itens);
        cardapio.setPreco(preco);
        cardapio.setIdFornecedor(mLoginDataSource.getUserLogged().getFornecedor().getId());
        mCardapioDataSource.saveCard(true, cardapio, this);
    }

    @Override
    public Context getContext() {
        return ((Fragment)mView).getActivity();
    }

    @Override
    public void start() {

    }

    @Override
    public void onCardStored() {
        mView.ocultaProgresso();
        mView.exibeMensagem("Cardapio cadastrado com sucesso", true);
    }

    @Override
    public void onDataNotStored() {
        mView.ocultaProgresso();
        mView.exibeMensagem("Falha ao conectar ao servidor", false);
    }
}
