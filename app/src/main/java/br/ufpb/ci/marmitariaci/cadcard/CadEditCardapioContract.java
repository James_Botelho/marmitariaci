package br.ufpb.ci.marmitariaci.cadcard;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface CadEditCardapioContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
    }

    interface Presenter extends BasePresenter{
        void salvar(String...strings);
        Context getContext();
    }
}
