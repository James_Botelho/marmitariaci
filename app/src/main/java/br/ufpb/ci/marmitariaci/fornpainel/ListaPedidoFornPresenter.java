package br.ufpb.ci.marmitariaci.fornpainel;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;
import br.ufpb.ci.marmitariaci.data.source.PedidoDataSource;

public class ListaPedidoFornPresenter implements ListaPedidoFornContract.Presenter, PedidoDataSource.GetDataCallback {

    private final PedidoDataSource pedidoDataSource;
    private final LoginDataSource mLoginDataSource;
    private final ListaPedidoFornContract.View mView;
    private boolean mNetwork;
    private static boolean faltaAcessar;
    private static List<Pedido> listaPedidoTemp;

    public ListaPedidoFornPresenter(PedidoDataSource pedidoDataSource, LoginDataSource loginDataSource, ListaPedidoFornContract.View view, boolean network){
        this.pedidoDataSource = pedidoDataSource;
        this.mLoginDataSource = loginDataSource;
        this.mView = view;
        mNetwork = network;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        pedidoDataSource.getPedidos(mNetwork, mLoginDataSource.getUserLogged().getFornecedor().getId(), this);
    }

    @Override
    public void onDataLoaded(List<Pedido> pedidos) {
        if(mNetwork){
            faltaAcessar = true;
            listaPedidoTemp = pedidos;
            pedidoDataSource.deleteAll(this);
            mNetwork = false;
        }else if(faltaAcessar){
            pedidoDataSource.saveAll(listaPedidoTemp, this);
            faltaAcessar = false;
        }else{
            if(listaPedidoTemp != null)
                mView.setListaPedido(listaPedidoTemp);
            else
                pedidoDataSource.getPedidos(false, 0, this);
        }
    }

    @Override
    public void onDataNotAvailable() {
        mView.exibeMensagem("Não há pedidos a serem listados", false);
    }
}
