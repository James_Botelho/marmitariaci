package br.ufpb.ci.marmitariaci.cadeditforn;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class CadEditFornecedorFragment extends Fragment implements CadEditFornContract.View {

    private TextView mNomeEmpresa;
    private TextView mLogin;
    private TextView mSenha;
    private TextView mTelefone;
    private TextView mEndereco;
    private Button botaoSalvar;
    private ProgressDialog dialogoProgresso;

    public static final String ARGUMENTO_FORNECEDOR = "dadosfornecedor";

    private CadEditFornContract.Presenter mPresenter;

    public CadEditFornecedorFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cad_edit_fornecedor, container, false);

        mNomeEmpresa = view.findViewById(R.id.editTextEmpresaId);
        mLogin = view.findViewById(R.id.editTextLoginEmpresaId);
        mSenha = view.findViewById(R.id.editTextSenhaEmpresaId);
        mTelefone = view.findViewById(R.id.editTextTelefoneEmpresaId);
        mEndereco = view.findViewById(R.id.editTextEnderecoEmpresaId);
        botaoSalvar = view.findViewById(R.id.cadFornButtonId);
        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomeEmpresa = mNomeEmpresa.getText().toString();
                String login = mLogin.getText().toString();
                String senha = mSenha.getText().toString();
                String telefone = mTelefone.getText().toString();
                String endereco = mEndereco.getText().toString();
                mPresenter.salva(nomeEmpresa, login, senha, telefone, endereco);
            }
        });
        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void exibeProgresso() {
        dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void setEndereco(String endereco) {
        mEndereco.setText(endereco);
    }

    @Override
    public void setTextNomeEmpresa(String nomeEmpresa) {
        mNomeEmpresa.setText(nomeEmpresa);
    }

    @Override
    public void setTextUsuario(String usuario) {
        mLogin.setText(usuario);
    }

    @Override
    public void setTextTelefone(String telefone) {
        mTelefone.setText(telefone);
    }

    @Override
    public void setTextHintSenha(String senha) {
        mSenha.setHint(senha);
    }

    @Override
    public void setTextButton(String texto) {
        if(texto != null){
            botaoSalvar.setText(texto);
        }
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
        if(encerra)
            getActivity().finish();
    }

    @Override
    public void setPresenter(CadEditFornContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
