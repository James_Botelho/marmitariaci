package br.ufpb.ci.marmitariaci.data.source.local;

import android.content.Context;
import android.content.SharedPreferences;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.Login;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;

public class LoginLocalDataSource implements LoginDataSource {

    private static final String FILE_PREF = "marm.prefs";
    private static final String TOKEN = "TOKEN_KEY";
    private static final String NOME = "NOME_KEY";
    private static final String TELEFONE = "TELEFONE_KEY";
    private static final String USUARIO = "USER_KEY";
    private static final String ENDERECO = "ENDERECO_KEY";
    private static final String CLIENT_KEY = "IS_CLIENT_KEY";
    private static final String ID = "ID_KEY";

    private static volatile LoginLocalDataSource INSTANCE;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private LoginLocalDataSource(Context context){
        preferences = context.getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static LoginLocalDataSource getInstance(Context context){
        if(INSTANCE == null){
            synchronized (LoginLocalDataSource.class){
                INSTANCE = new LoginLocalDataSource(context);
            }
        }
        return INSTANCE;
    }

    @Override
    public void loginUser(Credencial credencial, LoginUserCallback callback) {
        //Não é necessário implementar
    }

    @Override
    public void saveUserLogged(Login login) {
        editor.putString(TOKEN, login.getToken());
        if(login.getCliente() != null){
            editor.putInt(ID, login.getCliente().getId());
            editor.putString(NOME, login.getCliente().getNomeUsuario());
            editor.putString(USUARIO, login.getCliente().getUsuario());
            editor.putString(TELEFONE, login.getCliente().getTelefone());
            editor.putBoolean(CLIENT_KEY, true);
        }else{
            editor.putInt(ID, login.getFornecedor().getId());
            editor.putString(NOME, login.getFornecedor().getNomeEmpresa());
            editor.putString(USUARIO, login.getFornecedor().getUsuario());
            editor.putString(TELEFONE, login.getFornecedor().getTelefone());
            editor.putString(ENDERECO, login.getFornecedor().getEndereco());
            editor.putBoolean(CLIENT_KEY, false);
        }
        editor.commit();
    }

    @Override
    public Login getUserLogged() {

        //Não há usuário no sharedpreferences, necessário fazer login
        if(preferences.getInt(ID, 0) == 0)
            return null;

        int id = preferences.getInt(ID, 0);
        String nome = preferences.getString(NOME, null);
        String usuario = preferences.getString(USUARIO, null);
        String telefone = preferences.getString(TELEFONE, null);
        Login login = new Login();
        login.setToken(preferences.getString(TOKEN, null));
        if(preferences.getBoolean(CLIENT_KEY, true)){
            Cliente c = new Cliente();
            c.setId(id);
            c.setNomeUsuario(nome);
            c.setUsuario(usuario);
            c.setTelefone(telefone);
            login.setCliente(c);
        } else {
            Fornecedor f = new Fornecedor();
            f.setId(id);
            f.setNomeEmpresa(nome);
            f.setUsuario(usuario);
            f.setTelefone(telefone);
            f.setEndereco(preferences.getString(ENDERECO, null));
            login.setFornecedor(f);
        }
        return login;
    }

    @Override
    public void logoutUser() {
        editor.putInt(ID, 0);
        editor.commit();
    }
}
