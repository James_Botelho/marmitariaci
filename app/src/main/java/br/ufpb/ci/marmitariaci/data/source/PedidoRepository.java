package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.data.source.local.FornecedorLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.local.PedidoLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.FornecedorRemoteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.PedidoRemoteDataSource;

public class PedidoRepository implements PedidoDataSource {

    private static PedidoRepository INSTANCE = null;
    private final PedidoDataSource mPedidoRemoteDataSource;
    private final PedidoDataSource mPedidoLocalDataSource;

    private PedidoRepository(PedidoDataSource remoto, PedidoDataSource local){
        mPedidoRemoteDataSource = remoto;
        mPedidoLocalDataSource = local;
    }

    public static PedidoRepository getInstance(PedidoRemoteDataSource remoto, PedidoLocalDataSource local){
        if(INSTANCE == null)
            INSTANCE = new PedidoRepository(remoto, local);
        return INSTANCE;
    }

    @Override
    public void getPedidos(boolean network, int id, GetDataCallback callback) {
        if(network)
            mPedidoRemoteDataSource.getPedidos(true, id, callback);
        else
            mPedidoLocalDataSource.getPedidos(true, id, callback);
    }

    @Override
    public void savePedido(boolean network, Pedido pedido, SavePedCallback callback) {
        mPedidoRemoteDataSource.savePedido(network, pedido, callback);
    }

    @Override
    public void deleteAll(GetDataCallback callback) {
        mPedidoLocalDataSource.deleteAll(callback);
    }

    @Override
    public void saveAll(List<Pedido> pedidoList, GetDataCallback callback) {
        mPedidoLocalDataSource.saveAll(pedidoList, callback);
    }

    @Override
    public void deletePedido(int id, SavePedCallback callback) {
        mPedidoRemoteDataSource.deletePedido(id, callback);
    }
}
