package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Fornecedor;

public class FornecedorAdapter extends RecyclerView.Adapter<FornecedorAdapter.FornecedorViewHolder> {
    private List<Fornecedor> mListaFornecedor;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public FornecedorAdapter(Context context, List<Fornecedor> listaFornecedor){
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mListaFornecedor = listaFornecedor;
    }

    @NonNull
    @Override
    public FornecedorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_fornecedor, parent, false);
        FornecedorViewHolder fornecedorViewHolder = new FornecedorViewHolder(view);
        return fornecedorViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FornecedorViewHolder holder, int position) {
        String mensagemTemplate = mContext.getString(R.string.nome_lista_label);
        String valorLista = mListaFornecedor.get(position).getNomeEmpresa();
        holder.mNomeFornecedor.setText(String.format(mensagemTemplate, valorLista));

        mensagemTemplate = mContext.getString(R.string.telefone_lista_label);
        valorLista = mListaFornecedor.get(position).getTelefone();
        holder.mTelefoneFornecedor.setText(String.format(mensagemTemplate, valorLista));
    }

    @Override
    public int getItemCount() {
        return mListaFornecedor.size();
    }


    public class FornecedorViewHolder extends RecyclerView.ViewHolder{

        public TextView mNomeFornecedor;
        public TextView mTelefoneFornecedor;

        public FornecedorViewHolder(@NonNull View itemView) {
            super(itemView);

            mNomeFornecedor = itemView.findViewById(R.id.tituloFornecedorId);
            mTelefoneFornecedor = itemView.findViewById(R.id.tituloTelefoneId);
        }
    }
}
