package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.FornecedorDataSource;

public class ListaFornecedorPresenter implements ListaFornecedorContract.Presenter, FornecedorDataSource.LoadFornsCallback {

    //Referência para o repositório de fornecedores
    private final FornecedorDataSource mFornecedorDataSource;

    //Referência para o fragment com a visualização
    private final ListaFornecedorContract.View mView;

    //Variável para controle de acesso remoto
    private boolean mNetwork;

    //Controle de acesso ao bd para deletar, já que é assíncrono
    private static boolean faltaAcessar;

    //Lista de fornecedores temporária para auxílio na exclusão e inserção de fornecedores no banco
    private static List<Fornecedor> listaFornecedorTemp;

    public ListaFornecedorPresenter(FornecedorDataSource fornecedorDataSource, ListaFornecedorContract.View view, boolean network){
        this.mFornecedorDataSource = fornecedorDataSource;
        this.mView = view;
        this.mNetwork = network;
        view.setPresenter(this);
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getActivity();
    }

    @Override
    public void start() {
        mView.exibeProgresso();
        mFornecedorDataSource.getForns(mNetwork, this);
    }

    @Override
    public void onFornsLoaded(List<Fornecedor> forns) {
        if(mNetwork){
            faltaAcessar = true;
            listaFornecedorTemp = forns;
            mFornecedorDataSource.deleteAllForns(this);
            mNetwork = false;
        }else if(faltaAcessar){
            mFornecedorDataSource.saveAllForns(listaFornecedorTemp, this);
            faltaAcessar = false;
        } else {
            mView.ocultaProgresso();
            if(listaFornecedorTemp != null){
                mView.setListaFornecedor(listaFornecedorTemp);
            } else {
                mView.setListaFornecedor(forns);
            }
        }
    }

    @Override
    public void onDataNotAvailable() {
        mView.exibeMensagem(getContext().getString(R.string.cardapio_lista_erro), false);
    }
}
