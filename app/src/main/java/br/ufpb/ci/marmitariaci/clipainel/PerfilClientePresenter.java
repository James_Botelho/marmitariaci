package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;

public class PerfilClientePresenter implements PerfilClienteContract.Presenter {

    private final PerfilClienteContract.View mView;
    private final LoginDataSource mLoginDataSource;

    public PerfilClientePresenter(PerfilClienteContract.View mView, LoginDataSource loginDataSource) {
        this.mView = mView;
        this.mLoginDataSource = loginDataSource;
        mView.setPresenter(this);
    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void start() {
        Cliente cliente = mLoginDataSource.getUserLogged().getCliente();
        mView.setDadosPerfilCliente(cliente.getNomeUsuario(), cliente.getUsuario(), cliente.getTelefone());
    }
}
