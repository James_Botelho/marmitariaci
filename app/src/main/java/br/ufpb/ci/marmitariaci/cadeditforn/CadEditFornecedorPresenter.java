package br.ufpb.ci.marmitariaci.cadeditforn;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.FornecedorDataSource;

public class CadEditFornecedorPresenter implements CadEditFornContract.Presenter, FornecedorDataSource.SaveFornCallback {

    private final FornecedorDataSource mFornecedorDataSource;

    @NonNull
    private final CadEditFornContract.View mView;

    @Nullable
    private Fornecedor mFornecedor;

    public CadEditFornecedorPresenter(@NonNull CadEditFornContract.View mView, FornecedorDataSource dataSource, Fornecedor fornecedor) {
        this.mView = mView;
        this.mFornecedorDataSource = dataSource;
        this.mFornecedor = fornecedor;
        mView.setPresenter(this);
    }

    @Override
    public void salva(String... strings) {
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setNomeEmpresa(strings[0]);
        fornecedor.setUsuario(strings[1]);
        fornecedor.setSenha(strings[2]);
        fornecedor.setTelefone(strings[3]);
        fornecedor.setEndereco(strings[4]);
        if(isNewForn()){
            mFornecedorDataSource.saveForn(true, fornecedor, this);
        }else{
            fornecedor.setId(mFornecedor.getId());
        }
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getContext();
    }

    @Override
    public void start() {
        if(!isNewForn()){
            mView.setTextTelefone(mFornecedor.getTelefone());
            mView.setTextNomeEmpresa(mFornecedor.getNomeEmpresa());
            mView.setTextUsuario(mFornecedor.getUsuario());
            mView.setTextHintSenha("Insira a nova senha");
            mView.setEndereco(mFornecedor.getEndereco());
            mView.setTextButton("Editar");
        }
    }


    private boolean isNewForn(){
        return mFornecedor == null;
    }

    @Override
    public void onFornStored(Fornecedor f) {
        mView.ocultaProgresso();
        mView.exibeMensagem("Operação realizada com sucesso", true);
    }

    @Override
    public void onDataNotStored() {
        mView.ocultaProgresso();
        mView.exibeMensagem("Erro ao realizar operação", false);
    }
}
