package br.ufpb.ci.marmitariaci.telainiciallogin;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface TelaLoginContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
        void exibeDialogo();
        void exibeTelaCadastro(Class<?> cls);
        void exibePainelLogado(Class<?> cls);
        boolean estaAtiva();
    }

    interface Presenter extends BasePresenter{
        void logar(String...dados);
        void opcaoCadastro(int opcao);
        Context getContext();
    }
}
