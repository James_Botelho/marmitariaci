package br.ufpb.ci.marmitariaci.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;

@Dao
public interface PedidoDAO {

    @Insert
    void insertAll(List<Pedido> pedidos);

    @Query("SELECT * FROM Pedido")
    List<Pedido> getAll();

    @Query("DELETE FROM Pedido")
    void deleteAll();
}
