package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import br.ufpb.ci.marmitariaci.Injection;

public class TabFornAdapter extends FragmentStatePagerAdapter {

    private ListaCardapioFornPresenter mListaCardapioFornPresenter;
    private ListaPedidoFornPresenter mListaPedidoFornPresenter;
    private PerfilFornPresenter mPerfilFornPresenter;
    private String nomeTabs[];
    private Context mContext;
    private boolean mNetwork;
    private static int controleNetwork;

    public TabFornAdapter(FragmentManager fm, String nomeTabs[], Context context, boolean network) {
        super(fm);
        this.nomeTabs = nomeTabs;
        this.mContext = context;
        this.mNetwork = network;
        controleNetwork = 0;
    }

    private synchronized void contadorControleNetwork(){
        controleNetwork++;
        if(controleNetwork >= 3)
            this.mNetwork = false;
    }

    @Override
    public Fragment getItem(int position) {
        if(position != 2)
            contadorControleNetwork();
        switch (position){
            case 0:
                ListaCardapioFornFragment listaCardapioFornFragment = ListaCardapioFornFragment.newInstance();
                mListaCardapioFornPresenter = new ListaCardapioFornPresenter(Injection.getCardapioRepository(mContext), Injection.getLoginRepository(mContext), listaCardapioFornFragment, mNetwork);
                return listaCardapioFornFragment;
            case 1:
                ListaPedidoFornFragment listaPedidoFornFragment = new ListaPedidoFornFragment();
                mListaPedidoFornPresenter = new ListaPedidoFornPresenter(Injection.getPedidoRepository(mContext), Injection.getLoginRepository(mContext), listaPedidoFornFragment, mNetwork);
                return listaPedidoFornFragment;
            case 2:
                PerfilFornFragment perfilFornFragment = PerfilFornFragment.newInstance();
                mPerfilFornPresenter = new PerfilFornPresenter(Injection.getLoginRepository(mContext), perfilFornFragment);
                return perfilFornFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return nomeTabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return nomeTabs[position];
    }
}
