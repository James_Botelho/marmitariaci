package br.ufpb.ci.marmitariaci.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cliente;

@Dao
public interface ClienteDAO {

    @Insert
    void insertAllClients(List<Cliente> clients);

    @Update
    void updateAllClients(List<Cliente> clients);

    @Query("SELECT * FROM Cliente WHERE id = :id")
    Cliente getById(int id);

    @Query("DELETE FROM Cliente")
    void deleteAllClients();
}
