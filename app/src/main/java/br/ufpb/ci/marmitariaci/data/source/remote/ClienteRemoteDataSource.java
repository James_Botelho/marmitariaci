package br.ufpb.ci.marmitariaci.data.source.remote;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.source.ClienteDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.ClienteService;
import br.ufpb.ci.marmitariaci.data.source.remote.retrofit.RestCli;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClienteRemoteDataSource implements ClienteDataSource {
    private static volatile ClienteRemoteDataSource INSTANCE;

    private ClienteService clienteService;

    private ClienteRemoteDataSource(){
        clienteService = RestCli.getService();
    }

    public static ClienteRemoteDataSource getInstance(){
        if(INSTANCE == null)
            INSTANCE = new ClienteRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getClients(final LoadClientsCallback callback) {
        clienteService.listaClientes(HttpUtil.getToken()).enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                if(response.code() == 200){
                    callback.onClientsLoaded(response.body());
                }else{
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getClient(Integer id, GetClientCallback callback) {

    }

    @Override
    public void saveClient(Cliente c, final SaveClientCallback callback) {
        clienteService.adicionaCliente(c).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200){
                    callback.onClientStored();
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void updateClient(Cliente c, final SaveClientCallback callback) {
        clienteService.atualizaCliente(HttpUtil.getToken(), c, c.getId()).enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.code() == 200){
                    callback.onClientStored();
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }

    @Override
    public void refreshClients(LoadClientsCallback callback) {

    }

    @Override
    public void deleteAllClients(LoadClientsCallback callback) {

    }

    @Override
    public void deleteClient(Integer id, final SaveClientCallback callback) {
        clienteService.deletaCliente(HttpUtil.getToken(), id).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200){
                    callback.onClientStored();
                }else{
                    callback.onDataNotStored();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onDataNotStored();
            }
        });
    }
}
