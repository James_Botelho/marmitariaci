package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Intent;

import java.util.List;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;
import br.ufpb.ci.marmitariaci.data.Pedido;

public interface ListaPedidoFornContract {

    interface View extends BaseView<Presenter> {
        void setListaPedido(List<Pedido> listaPedido);
    }

    interface Presenter extends BasePresenter {

    }
}
