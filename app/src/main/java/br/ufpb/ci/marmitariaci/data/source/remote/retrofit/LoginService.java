package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Login;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {
    @POST("login")
    Call<Login> logaUsuario(@Body Credencial credencial);
}
