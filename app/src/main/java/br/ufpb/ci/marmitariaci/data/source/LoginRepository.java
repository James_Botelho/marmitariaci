package br.ufpb.ci.marmitariaci.data.source;

import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Login;
import br.ufpb.ci.marmitariaci.data.source.local.LoginLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.LoginRemoteDataSource;

public class LoginRepository implements LoginDataSource {

    private static LoginRepository INSTANCE;

    private final LoginLocalDataSource mLoginLocalDataSource;
    private final LoginRemoteDataSource mLoginRemoteDataSource;

    private LoginRepository(LoginLocalDataSource local, LoginRemoteDataSource remoto){
        mLoginLocalDataSource = local;
        mLoginRemoteDataSource = remoto;
    }

    public static LoginRepository getInstance(LoginLocalDataSource local, LoginRemoteDataSource remoto){
        if(INSTANCE == null)
            INSTANCE = new LoginRepository(local, remoto);
        return INSTANCE;
    }

    @Override
    public void loginUser(Credencial credencial, LoginUserCallback callback) {
        mLoginRemoteDataSource.loginUser(credencial, callback);
    }

    @Override
    public void saveUserLogged(Login login) {
        mLoginLocalDataSource.saveUserLogged(login);
    }

    @Override
    public Login getUserLogged() {
        return mLoginLocalDataSource.getUserLogged();
    }

    @Override
    public void logoutUser() {
        mLoginLocalDataSource.logoutUser();
    }
}
