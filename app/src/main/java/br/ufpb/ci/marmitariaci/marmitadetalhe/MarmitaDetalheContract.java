package br.ufpb.ci.marmitariaci.marmitadetalhe;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface MarmitaDetalheContract {

    interface View extends BaseView<Presenter>{
        void setDados(String...dados);
        void exibeProgresso();
        void ocultaProgresso();
    }

    interface Presenter extends BasePresenter{
        void realizaPedido(String...dados);
    }
}
