package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.local.CardapioLocalDataSource;
import br.ufpb.ci.marmitariaci.data.source.remote.CardapioRemoteDataSource;

public class CardapioRepository implements CardapioDataSource {


    private static CardapioRepository INSTANCE = null;
    private final CardapioLocalDataSource mCardapioLocalDataSource;
    private final CardapioRemoteDataSource mCardapioRemoteDataSource;

    private CardapioRepository(CardapioLocalDataSource local, CardapioRemoteDataSource remoto){
        mCardapioLocalDataSource = local;
        mCardapioRemoteDataSource = remoto;
    }

    public static CardapioRepository getInstance(CardapioLocalDataSource local, CardapioRemoteDataSource remoto){
        if(INSTANCE == null)
            INSTANCE = new CardapioRepository(local, remoto);
        return INSTANCE;
    }

    @Override
    public void getCards(boolean network, LoadCardsCallback callback) {
        if (network)
            mCardapioRemoteDataSource.getCards(network, callback);
        else
            mCardapioLocalDataSource.getCards(network, callback);
    }

    @Override
    public void getCard(boolean network, Integer id, GetCardCallback callback) {
        //Não é necessário pegar o cardápio remoto, já está na base de dados
        mCardapioLocalDataSource.getCard(network, id, callback);
    }

    @Override
    public void getCardFornecedor(Integer id, LoadCardsCallback callback) {
        mCardapioRemoteDataSource.getCardFornecedor(id, callback);
    }

    @Override
    public void saveCard(boolean network, Cardapio c, SaveCardCallback callback) {
        if (network)
            mCardapioRemoteDataSource.saveCard(network, c, callback);
        else
            mCardapioLocalDataSource.saveCard(network, c, callback);
    }

    @Override
    public void saveAllCard(List<Cardapio> cards, LoadCardsCallback callback) {
        mCardapioLocalDataSource.saveAllCard(cards, callback);
    }

    @Override
    public void refreshCards(LoadCardsCallback callback) {

    }

    @Override
    public void deleteAllCards(LoadCardsCallback callback) {
        mCardapioLocalDataSource.deleteAllCards(callback);
    }

    @Override
    public void deleteCard(boolean network, Integer id, SaveCardCallback callback) {
        mCardapioRemoteDataSource.deleteCard(network, id, callback);
    }
}
