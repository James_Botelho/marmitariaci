package br.ufpb.ci.marmitariaci.cadeditcli;

import android.content.Context;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;

public interface CadEditCliContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
        void setTextButton(String texto);
        void setTextNome(String nome);
        void setTextUsuario(String usuario);
        void setTextTelefone(String telefone);
        void setTextHintSenha(String senha);
    }

    interface Presenter extends BasePresenter{
        void salvar(String... strings);
        Context getContext();
    }
}
