package br.ufpb.ci.marmitariaci.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;

@Dao
public interface CardapioDAO {
    @Insert
    void insertAll(List<Cardapio> cards);

    @Update
    void updateAll(List<Cardapio> cards);

    @Query("SELECT * FROM Cardapio WHERE id = :id")
    Cardapio getById(int id);

    @Query("SELECT * FROM Cardapio")
    List<Cardapio> getAll();

    @Query("DELETE FROM Cardapio")
    void deleteAll();
}
