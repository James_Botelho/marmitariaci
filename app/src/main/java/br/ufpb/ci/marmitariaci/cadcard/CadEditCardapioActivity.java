package br.ufpb.ci.marmitariaci.cadcard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import br.ufpb.ci.marmitariaci.Injection;
import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.utils.ActivityUtil;

public class CadEditCardapioActivity extends AppCompatActivity {

    private CadEditCardapioPresenter mCadEditCardapioPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_edit_cardapio);

        CadEditCardapioFragment cadEditCardapioFragment = (CadEditCardapioFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if(cadEditCardapioFragment == null){
            cadEditCardapioFragment = CadEditCardapioFragment.newInstance();

            ActivityUtil.addFragmentToActivity(getSupportFragmentManager(), cadEditCardapioFragment, R.id.contentFrame);
        }

        mCadEditCardapioPresenter = new CadEditCardapioPresenter(cadEditCardapioFragment, Injection.getCardapioRepository(getApplicationContext()), Injection.getLoginRepository(getApplicationContext()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
