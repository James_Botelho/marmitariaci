package br.ufpb.ci.marmitariaci.fornpainel;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;
import br.ufpb.ci.marmitariaci.cadcard.CadEditCardapioActivity;
import br.ufpb.ci.marmitariaci.clipainel.CardapioAdapter;
import br.ufpb.ci.marmitariaci.clipainel.ListaCardapioContract;
import br.ufpb.ci.marmitariaci.data.Cardapio;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaCardapioFornFragment extends Fragment implements ListaCardapioFornContract.View, RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ListaCardapioFornContract.Presenter mPresenter;
    private ProgressDialog dialogoProgresso;

    public static ListaCardapioFornFragment newInstance(){
        return new ListaCardapioFornFragment();
    }

    public ListaCardapioFornFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cardapio, container, false);

        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);

        mRecyclerView = view.findViewById(R.id.rv_cardapio);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(llm);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setListaCardapio(List<Cardapio> listaCardapio) {
        CardapioAdapter adapter = new CardapioAdapter(getActivity(), listaCardapio);
        adapter.setRecyclerViewOnClickListenerHack(this);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void iniciaActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(ListaCardapioFornContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lista_marmita_fornecedor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void exibeDialogo(final Cardapio c) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Selecione uma opção");
        builder.setItems(getResources().getStringArray(R.array.visualiza_tipo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mPresenter.opcaoSelecionada(i, c);
            }
        });
        builder.show();
    }

    @Override
    public void onClickListener(View view, int position) {
        CardapioAdapter adapter = (CardapioAdapter) mRecyclerView.getAdapter();
        Cardapio c = adapter.getItem(position);
        exibeDialogo(c);
    }
}
