package br.ufpb.ci.marmitariaci.fornpainel;

import android.content.Context;
import android.support.v4.app.Fragment;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;

public class PerfilFornPresenter implements PerfilFornContract.Presenter {

    private final LoginDataSource mLoginDataSource;
    private final PerfilFornContract.View mView;

    public PerfilFornPresenter(LoginDataSource mLoginDataSource, PerfilFornContract.View mView) {
        this.mLoginDataSource = mLoginDataSource;
        this.mView = mView;
        mView.setPresenter(this);
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getActivity();
    }

    @Override
    public void start() {
        Fornecedor f = mLoginDataSource.getUserLogged().getFornecedor();
        mView.setDados(f.getNomeEmpresa(), f.getUsuario(), f.getEndereco(), f.getTelefone());
    }
}
