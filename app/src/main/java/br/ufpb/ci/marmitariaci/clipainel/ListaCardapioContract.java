package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;

import java.util.List;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;
import br.ufpb.ci.marmitariaci.data.Cardapio;

public interface ListaCardapioContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
        void setListaCardapio(List<Cardapio> listaCardapio);
        boolean estaAtiva();
    }

    interface Presenter extends BasePresenter{
        Context getContext();
    }
}
