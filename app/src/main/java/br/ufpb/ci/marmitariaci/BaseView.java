package br.ufpb.ci.marmitariaci;

public interface BaseView<T> {
    void exibeMensagem(String mensagem, boolean encerra);
    void setPresenter(T t);
}
