package br.ufpb.ci.marmitariaci.utils;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class ConverteData {
    @TypeConverter
    public static Date converteParaData(Long valor){
        return new Date(valor);
    }

    @TypeConverter
    public static Long converteParaLong(Date data){
        return 0L;
    }
}
