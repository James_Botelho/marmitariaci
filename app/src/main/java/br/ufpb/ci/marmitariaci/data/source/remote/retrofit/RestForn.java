package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ufpb.ci.marmitariaci.utils.HttpUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestForn {
    private static FornecedorService service = null;

    private static FornecedorService initFornService(){
        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(HttpUtil.getURL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(FornecedorService.class);
    }

    public static FornecedorService getService(){
        if(service == null)
            service = initFornService();
        return service;
    }
}
