package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.CardapioDataSource;

public class ListaCardapioPresenter implements ListaCardapioContract.Presenter, CardapioDataSource.LoadCardsCallback {

    private final CardapioDataSource mCardapioDataSource;

    private final ListaCardapioContract.View mView;

    private boolean mNetwork;

    private static boolean faltaAcessar; //Controle de acesso ao bd para deletar, já que é assíncrono

    private static List<Cardapio> listaCardapioTemp;

    public ListaCardapioPresenter(CardapioDataSource cardapioDataSource, ListaCardapioContract.View view, boolean network) {
        this.mCardapioDataSource = cardapioDataSource;
        this.mView = view;
        this.mNetwork = network;
        view.setPresenter(this);
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getActivity();
    }

    @Override
    public void start() {
        mView.exibeProgresso();
        mCardapioDataSource.getCards(mNetwork, this);
    }

    @Override
    public void onCardsLoaded(List<Cardapio> cards) {
        if(mNetwork){
            faltaAcessar = true;
            listaCardapioTemp = cards;
            mNetwork = false;
            mCardapioDataSource.deleteAllCards(this);
        }else if(faltaAcessar){
            faltaAcessar = false;
            mCardapioDataSource.saveAllCard(listaCardapioTemp, this);
        } else {
            mView.ocultaProgresso();
            if(listaCardapioTemp != null)
                mView.setListaCardapio(listaCardapioTemp);
            else
                mView.setListaCardapio(cards);
        }
    }

    @Override
    public void onDataNotAvailable() {
        mView.exibeMensagem(getContext().getString(R.string.cardapio_lista_erro), false);
    }
}
