package br.ufpb.ci.marmitariaci.clipainel;

import android.content.Context;

import java.util.List;

import br.ufpb.ci.marmitariaci.BasePresenter;
import br.ufpb.ci.marmitariaci.BaseView;
import br.ufpb.ci.marmitariaci.data.Fornecedor;

public interface ListaFornecedorContract {

    interface View extends BaseView<Presenter>{
        void exibeProgresso();
        void ocultaProgresso();
        void setListaFornecedor(List<Fornecedor> listaFornecedor);
        boolean estaAtiva();
    }

    interface Presenter extends BasePresenter{
        Context getContext();
    }
}
