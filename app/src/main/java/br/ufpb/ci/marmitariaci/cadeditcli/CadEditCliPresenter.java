package br.ufpb.ci.marmitariaci.cadeditcli;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.source.ClienteDataSource;
import br.ufpb.ci.marmitariaci.data.source.ClienteRepository;

public class CadEditCliPresenter implements CadEditCliContract.Presenter, ClienteDataSource.SaveClientCallback, ClienteDataSource.GetClientCallback {

    @NonNull
    private final ClienteDataSource mCliDataSource;

    @NonNull
    private final CadEditCliContract.View mCadEditCli;

    @Nullable
    private Integer mCliId;

    public CadEditCliPresenter(Integer cliId, ClienteDataSource cliDataSource, CadEditCliContract.View addCliView){
        mCliId = cliId;
        mCadEditCli = addCliView;
        mCliDataSource = cliDataSource;
        mCadEditCli.setPresenter(this);
    }

    @Override
    public void salvar(String... strings) {
        mCadEditCli.exibeProgresso();
        String nome = strings[0];
        String login = strings[1];
        String senha = strings[2];
        String telefone = strings[3];
        Cliente cliente = new Cliente();
        cliente.setNomeUsuario(nome);
        cliente.setUsuario(login);
        cliente.setSenha(senha);
        cliente.setTelefone(telefone);
        if(!isNewClient()){
            cliente.setId(mCliId);
            mCliDataSource.saveClient(cliente, this);
        }else{
            mCliDataSource.updateClient(cliente, this);
        }
    }

    @Override
    public Context getContext() {
        return ((Fragment) mCadEditCli).getContext();
    }

    @Override
    public void start() {
        if(!isNewClient()){
            //Pegar Cliente do SharedPreferences
            mCadEditCli.setTextButton(getContext().getString(R.string.editar_botao));
        }
    }

    private boolean isNewClient(){
        //Verifica se o cliente é novo, ou seja, o Id = nulo
        return mCliId == null;
    }

    @Override
    public void onClientLoaded(Cliente client) {
        //Chamado quando o cliente é carregado
    }

    @Override
    public void onDataNotAvailable() {
        //Chamado quando o cliente não é carregado
    }

    @Override
    public void onClientStored() {
        mCadEditCli.ocultaProgresso();
        mCadEditCli.exibeMensagem("Cadastro realizado com sucesso", true);
    }

    @Override
    public void onDataNotStored() {
        mCadEditCli.ocultaProgresso();
        mCadEditCli.exibeMensagem("Não foi possível salvar o cliente", false);
    }
}
