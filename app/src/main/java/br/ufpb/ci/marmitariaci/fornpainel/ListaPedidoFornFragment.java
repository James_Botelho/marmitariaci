package br.ufpb.ci.marmitariaci.fornpainel;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;
import br.ufpb.ci.marmitariaci.data.Pedido;
import br.ufpb.ci.marmitariaci.pedidodetalhe.DetalhesPedidoActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPedidoFornFragment extends Fragment implements ListaPedidoFornContract.View, RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ListaPedidoFornContract.Presenter mPresenter;

    public static ListaPedidoFornFragment newInstance(){
        return new ListaPedidoFornFragment();
    }

    public ListaPedidoFornFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_pedido, container, false);

        mRecyclerView = view.findViewById(R.id.rv_pedido);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(llm);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setListaPedido(List<Pedido> listaPedido) {
        ListaPedidoAdapter adapter = new ListaPedidoAdapter(getActivity(), listaPedido);
        adapter.setmRecyclerViewOnClickListenerHack(this);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(ListaPedidoFornContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onClickListener(View view, int position) {
        ListaPedidoAdapter adapter = (ListaPedidoAdapter) mRecyclerView.getAdapter();
        Pedido pedido = adapter.getItem(position);
        Intent intent = new Intent(getActivity(), DetalhesPedidoActivity.class);
        intent.putExtra("pedido", pedido);
        startActivity(intent);
    }
}
