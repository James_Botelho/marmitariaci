package br.ufpb.ci.marmitariaci.cadeditcli;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.ufpb.ci.marmitariaci.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class CadEditClientFragment extends Fragment implements CadEditCliContract.View {

    public static final String ARGUMENT_EDIT_CLIENT_ID = "EDIT_CLIENT_ID";

    private CadEditCliContract.Presenter mPresenter;

    private TextView mNome;
    private TextView mLogin;
    private TextView mSenha;
    private TextView mTelefone;
    private Button mBotaoCadastrar;
    private ProgressDialog dialogoProgresso;

    public static CadEditClientFragment newInstance(){
        return new CadEditClientFragment();
    }

    public CadEditClientFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(CadEditCliContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_cad_edit_client, container, false);
        mNome = root.findViewById(R.id.editTextNomeCadCliId);
        mLogin = root.findViewById(R.id.editTextLoginCadCliId);
        mSenha = root.findViewById(R.id.editTextSenhaCadCliId);
        mTelefone = root.findViewById(R.id.editTextTelCadCliId);
        mBotaoCadastrar = root.findViewById(R.id.cadButtonCadCliId);
        mBotaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.salvar(mNome.getText().toString(), mLogin.getText().toString(), mSenha.getText().toString(), mTelefone.getText().toString());
            }
        });
        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);
        return root;
    }

    @Override
    public void exibeProgresso() {
        dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Toast.makeText(getActivity(), mensagem, Toast.LENGTH_SHORT).show();
        if(encerra)
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
    }

    @Override
    public void setTextButton(String texto) {
        mBotaoCadastrar.setText(texto);
    }

    @Override
    public void setTextNome(String nome) {
        mNome.setText(nome);
    }

    @Override
    public void setTextUsuario(String usuario) {
        mLogin.setText(usuario);
    }

    @Override
    public void setTextTelefone(String telefone) {
        mTelefone.setText(telefone);
    }

    @Override
    public void setTextHintSenha(String senha) {
        mSenha.setHint(senha);
    }
}
