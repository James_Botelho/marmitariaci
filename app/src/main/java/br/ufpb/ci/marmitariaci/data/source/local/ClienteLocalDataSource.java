package br.ufpb.ci.marmitariaci.data.source.local;

import android.os.AsyncTask;

import br.ufpb.ci.marmitariaci.data.Cliente;
import br.ufpb.ci.marmitariaci.data.source.ClienteDataSource;

public class ClienteLocalDataSource implements ClienteDataSource {

    private static volatile ClienteLocalDataSource INSTANCE;
    private ClienteDAO clienteDAO;

    private ClienteLocalDataSource(ClienteDAO dao){
        clienteDAO = dao;
    }

    public static ClienteLocalDataSource getInstance(ClienteDAO dao){
        if(INSTANCE == null){
            synchronized (ClienteLocalDataSource.class){
                INSTANCE = new ClienteLocalDataSource(dao);
            }
        }
        return INSTANCE;
    }

    @Override
    public void getClients(final LoadClientsCallback callback) {

    }

    @Override
    public void getClient(final Integer id, final GetClientCallback callback) {
        new AsyncTask<Void, Void,Cliente>(){
            @Override
            protected Cliente doInBackground(Void... voids) {
                return clienteDAO.getById(id);
            }

            @Override
            protected void onPostExecute(Cliente cliente) {
                if(cliente == null){
                    callback.onDataNotAvailable();
                }else{
                    callback.onClientLoaded(cliente);
                }
            }
        }.execute();
    }

    @Override
    public void saveClient(Cliente c, SaveClientCallback callback) {

    }

    @Override
    public void updateClient(Cliente c, SaveClientCallback callback) {

    }

    @Override
    public void refreshClients(LoadClientsCallback callback) {

    }

    @Override
    public void deleteAllClients(final LoadClientsCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                clienteDAO.deleteAllClients();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onClientsLoaded(null);
            }
        }.execute();
    }

    @Override
    public void deleteClient(Integer id, SaveClientCallback callback) {

    }
}
