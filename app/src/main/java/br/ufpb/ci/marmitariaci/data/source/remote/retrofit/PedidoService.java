package br.ufpb.ci.marmitariaci.data.source.remote.retrofit;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Pedido;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PedidoService {
    //GET, POST DELETE(ID)
    @GET("pedido")
    Call<List<Pedido>> listaPedido(@Header("Authorization") String jwt);

    @POST("pedido")
    Call<Void> adicionaPedido(@Body Pedido pedido, @Header("Authorization") String jwt);

    @DELETE("pedido/{id}")
    Call<Void> deletaPedido(@Path("id") int id, @Header("Authorization") String jwt);
}
