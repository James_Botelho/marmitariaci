package br.ufpb.ci.marmitariaci.data.source.local;

import android.os.AsyncTask;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cardapio;
import br.ufpb.ci.marmitariaci.data.source.CardapioDataSource;

public class CardapioLocalDataSource implements CardapioDataSource {

    private static volatile CardapioLocalDataSource INSTANCE;
    private CardapioDAO cardapioDAO;

    private CardapioLocalDataSource(CardapioDAO dao){
        cardapioDAO = dao;
    }

    public static CardapioLocalDataSource getInstance(CardapioDAO dao){
        if(INSTANCE == null){
            synchronized (CardapioLocalDataSource.class){
                INSTANCE = new CardapioLocalDataSource(dao);
            }
        }
        return INSTANCE;
    }

    @Override
    public void getCards(boolean network, final LoadCardsCallback callback) {
        new AsyncTask<Void, Void, List>(){
            @Override
            protected List doInBackground(Void... voids) {
                return cardapioDAO.getAll();
            }

            @Override
            protected void onPostExecute(List list) {
                if(list == null || list.isEmpty())
                    callback.onDataNotAvailable();
                else
                    callback.onCardsLoaded(list);
            }
        }.execute();
    }

    @Override
    public void getCard(boolean network, final Integer id, final GetCardCallback callback) {
        new AsyncTask<Void, Void, Cardapio>(){
            @Override
            protected Cardapio doInBackground(Void... voids) {
                return cardapioDAO.getById(id);
            }

            @Override
            protected void onPostExecute(Cardapio cardapio) {
                if(cardapio == null)
                    callback.onDataNotAvailable();
                else
                    callback.onCardLoaded(cardapio);
            }
        }.execute();
    }

    @Override
    public void getCardFornecedor(Integer id, LoadCardsCallback callback) {

    }

    @Override
    public void saveCard(boolean network, Cardapio c, SaveCardCallback callback) {

    }

    @Override
    public void saveAllCard(final List<Cardapio> cards, final LoadCardsCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                cardapioDAO.insertAll(cards);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onCardsLoaded(null);
            }
        }.execute();
    }

    @Override
    public void refreshCards(LoadCardsCallback callback) {

    }

    @Override
    public void deleteAllCards(final LoadCardsCallback callback) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                cardapioDAO.deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                callback.onCardsLoaded(null);
            }
        }.execute();
    }

    @Override
    public void deleteCard(boolean network, Integer id, SaveCardCallback callback) {

    }
}
