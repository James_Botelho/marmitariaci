package br.ufpb.ci.marmitariaci.data.source;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Cliente;

public interface ClienteDataSource {
    interface LoadClientsCallback{
        void onClientsLoaded(List<Cliente> clients);
        void onDataNotAvailable();
    }

    interface GetClientCallback{
        void onClientLoaded(Cliente client);
        void onDataNotAvailable();
    }

    interface SaveClientCallback{
        void onClientStored();
        void onDataNotStored();
    }

    void getClients(LoadClientsCallback callback);
    void getClient(Integer id, GetClientCallback callback);
    void saveClient(Cliente c, SaveClientCallback callback);
    void updateClient(Cliente c, SaveClientCallback callback);
    void refreshClients(LoadClientsCallback callback);
    void deleteAllClients(LoadClientsCallback callback);
    void deleteClient(Integer id, SaveClientCallback callback);
}
