package br.ufpb.ci.marmitariaci.telainiciallogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.ufpb.ci.marmitariaci.Injection;
import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.utils.ActivityUtil;

public class MainActivity extends AppCompatActivity {

    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if(mainFragment == null){
            mainFragment = MainFragment.newInstance();
            ActivityUtil.addFragmentToActivity(getSupportFragmentManager(), mainFragment, R.id.contentFrame);
        }

        mPresenter = new MainPresenter(mainFragment, Injection.getLoginRepository(getApplicationContext()));
    }
}
