package br.ufpb.ci.marmitariaci.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;

@Dao
public interface FornecedorDAO {
    @Insert
    void insertAll(List<Fornecedor> forns);

    @Update
    void updateAll(List<Fornecedor> forns);

    @Query("SELECT * FROM Fornecedor WHERE id = :id")
    Fornecedor getBYId(int id);

    @Query("SELECT * FROM Fornecedor")
    List<Fornecedor> getAll();

    @Query("DELETE FROM Fornecedor")
    void deleteAll();
}
