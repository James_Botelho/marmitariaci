package br.ufpb.ci.marmitariaci.data;

import java.io.Serializable;

public class Login implements Serializable {
    private String token;
    private Fornecedor fornecedor;
    private Cliente cliente;

    public Login(){
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
