package br.ufpb.ci.marmitariaci.telainiciallogin;

import android.content.Context;
import android.support.v4.app.Fragment;

import br.ufpb.ci.marmitariaci.cadeditcli.CadEditClientActivity;
import br.ufpb.ci.marmitariaci.cadeditforn.CadEditFornecedor;
import br.ufpb.ci.marmitariaci.clipainel.PainelCliActivity;
import br.ufpb.ci.marmitariaci.data.Credencial;
import br.ufpb.ci.marmitariaci.data.Login;
import br.ufpb.ci.marmitariaci.data.source.LoginDataSource;
import br.ufpb.ci.marmitariaci.data.source.LoginRepository;
import br.ufpb.ci.marmitariaci.fornpainel.PainelFornecedorActivity;
import br.ufpb.ci.marmitariaci.utils.HttpUtil;

public class MainPresenter implements TelaLoginContract.Presenter, LoginDataSource.LoginUserCallback {

    private final TelaLoginContract.View mView;

    private final LoginRepository mLoginRepository;

    public MainPresenter(TelaLoginContract.View mView, LoginRepository mLoginRepository) {
        this.mView = mView;
        this.mLoginRepository = mLoginRepository;
        mView.setPresenter(this);
    }

    @Override
    public void logar(String... dados) {
        mView.exibeProgresso();
        Credencial credencial = new Credencial(dados[0], dados[1]);

        mLoginRepository.loginUser(credencial, this);
    }

    @Override
    public void opcaoCadastro(int opcao) {
        if(opcao == 0){
            mView.exibeTelaCadastro(CadEditFornecedor.class);
        }else if(opcao == 1){
            mView.exibeTelaCadastro(CadEditClientActivity.class);
        }
    }

    @Override
    public Context getContext() {
        return ((Fragment) mView).getActivity().getApplicationContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void onUserLogged(Login login) {
        mLoginRepository.saveUserLogged(login);
        if(mView.estaAtiva()){
            mView.ocultaProgresso();
            if(login.getCliente() != null){
                mView.exibePainelLogado(PainelCliActivity.class);
                HttpUtil.setToken(login.getToken());
            }else{
                mView.exibePainelLogado(PainelFornecedorActivity.class);
                HttpUtil.setToken(login.getToken());
            }
        }
    }

    @Override
    public void onUserNotLogged(int codigo) {
        if(mView.estaAtiva()){
            mView.ocultaProgresso();
            mView.exibeMensagem(HttpUtil.getMensagem(getContext(), codigo), false);
        }
    }
}