package br.ufpb.ci.marmitariaci.clipainel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.ufpb.ci.marmitariaci.R;
import br.ufpb.ci.marmitariaci.RecyclerViewOnClickListenerHack;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilClienteFragment extends Fragment implements PerfilClienteContract.View, RecyclerViewOnClickListenerHack {

    private TextView nome;
    private TextView usuario;
    private TextView telefone;

    private PerfilClienteContract.Presenter mPresenter;

    public static PerfilClienteFragment newInstance(){
        return new PerfilClienteFragment();
    }

    public PerfilClienteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_perfil_cliente, container, false);

        nome = view.findViewById(R.id.nomePerfilTextId);
        usuario = view.findViewById(R.id.usuarioPerfilTextId);
        telefone =  view.findViewById(R.id.telefonePerfilTextId);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setDadosPerfilCliente(String... dados) {
        nome.setText(dados[0]);
        usuario.setText(dados[1]);
        telefone.setText(dados[2]);
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {

    }

    @Override
    public void setPresenter(PerfilClienteContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onClickListener(View view, int position) {

    }
}
