package br.ufpb.ci.marmitariaci.telainiciallogin;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.ufpb.ci.marmitariaci.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements TelaLoginContract.View {

    private TelaLoginContract.Presenter mPresenter;

    private TextView mLogin;
    private TextView mSenha;
    private Button mLogar;
    private Button mCadastrar;
    private ProgressDialog dialogoProgresso;

    public static MainFragment newInstance(){
        return new MainFragment();
    }


    public MainFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        mLogin = root.findViewById(R.id.loginTextId);
        mSenha = root.findViewById(R.id.senhaTextId);
        mLogar = root.findViewById(R.id.logarButtonId);
        mCadastrar = root.findViewById(R.id.buttonCadastrarId);
        dialogoProgresso = new ProgressDialog(getActivity());
        dialogoProgresso.setMessage(getString(R.string.dialogo_progresso));
        dialogoProgresso.setCancelable(false);
        dialogoProgresso.setCanceledOnTouchOutside(false);

        mLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.logar(mLogin.getText().toString(), mSenha.getText().toString());
            }
        });

        mCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exibeDialogo();
            }
        });

        return root;
    }

    @Override
    public void exibeProgresso() {
        dialogoProgresso.show();
    }

    @Override
    public void ocultaProgresso() {
        dialogoProgresso.dismiss();
    }

    @Override
    public void exibeDialogo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.titulo_dialogo));
        builder.setItems(getResources().getStringArray(R.array.tipo_cadastro_dialogo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mPresenter.opcaoCadastro(i);
            }
        });
        builder.show();
    }

    @Override
    public void exibeTelaCadastro(Class<?> cls) {
        Intent intent = new Intent(getContext(), cls);
        startActivity(intent);
    }

    @Override
    public void exibePainelLogado(Class<?> cls) {
        Intent intent = new Intent(getActivity(), cls);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void exibeMensagem(String mensagem, boolean encerra) {
        Snackbar.make(getActivity().findViewById(R.id.coordinatorLayout), mensagem, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(TelaLoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public boolean estaAtiva() {
        return isAdded();
    }
}
