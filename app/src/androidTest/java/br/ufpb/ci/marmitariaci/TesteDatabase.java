package br.ufpb.ci.marmitariaci;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ufpb.ci.marmitariaci.data.Fornecedor;
import br.ufpb.ci.marmitariaci.data.source.local.AppDataBase;
import br.ufpb.ci.marmitariaci.data.source.local.FornecedorDAO;

@RunWith(AndroidJUnit4.class)
public class TesteDatabase {
    private FornecedorDAO mFornDao;
    private AppDataBase mDb;

    @Before
    public void iniciaDb(){
        Context context = InstrumentationRegistry.getContext();
        mDb = Room.inMemoryDatabaseBuilder(context, AppDataBase.class).build();
        mFornDao = mDb.fornecedorDAO();
    }

    @After
    public void encerraDb(){
        mDb.close();
    }

    @Test
    public void insereFornecedorELe(){

        Date date = new Date();

        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setId(1);
        fornecedor.setEndereco("Rua sem nome");
        fornecedor.setTelefone("83999154081");
        fornecedor.setUsuario("james_");
        fornecedor.setNome("James Lanches");
        fornecedor.setSenha("12345678");
        fornecedor.setDate(date);

        List<Fornecedor> fornecedors = new ArrayList<>();
        fornecedors.add(fornecedor);

        mFornDao.insertAll(fornecedors);

        List<Fornecedor> noBanco = mFornDao.getAll();

        Assert.assertEquals(date, noBanco.get(0).getDate());
    }
}
